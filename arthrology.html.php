<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * Frontend Event Handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Includes

/******************************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S FRONT-END HTML FUNCTIONALITY  */
/******************************************************************************/
class clsArthrologyHTML {

	/******************/
	/*  Prepare HTML  */
	/******************/
	static public function prepareHTML() {
		global $mainframe, $objArthrology;
		
		// Print components css information and header inclusions
		$linktag = '<link href="'.$objArthrology->live_path.'/css/arthrology.css" rel="stylesheet" type="text/css" />';
		if (mosGetParam( $_REQUEST, 'pop', 0)) {
			echo $linktag;
		} else {
			$mainframe->addCustomHeadTag($linktag);
		}
		
		echo '<h1 class="contentheading">'.$objArthrology->lng->GEN_COMPONENT_TITLE.'</h1>';
	}

	/************************/
	/*  Show search screen  */
	/************************/
	static public function searchScreenHTML() {
		global $Itemid, $objArthrology;

		$menuitem = "&Itemid=".$Itemid;
		$seoAction = sefRelToAbs('index.php?option=com_arthrology&task=results'.$menuitem, ARTHBASE."/results.html");
		?>
		
		<div class="arthrology_searchbox">
			<form action="<?php echo $seoAction ?>" method="post" name="hmtlForm">
			
				<?php echo $objArthrology->lng->SRCH_KEYW_ENTER; ?> <br/><br/>
				
				<table><tr>
					<td>
						<b><?php echo $objArthrology->lng->SRCH_MAG; ?>:</b>
					</td><td>
						<?php echo mosAdminMenus::ComponentCategory('catid','com_arthrology'); ?>
					</td>
				</tr><tr>
					<td>
						<b><?php echo $objArthrology->lng->SRCH_KEYW; ?>:</b>
					</td><td>
						<input type="text" name="keyword" value="" class="inputbox" size="40" />
					</td>				
				</tr></table>

				<input type="hidden" name="task" value="results" />
				<input type="hidden" name="option" value="com_arthrology" />
				<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
				
				<p align="center">
					<input type="submit" value="<?php echo _E_SEARCH; ?>" title="<?php echo _E_SEARCH; ?>" class="button" />
				</p>	
			</form>
		</div>
		<?php
	}
	
	/******************/
	/*  Show results  */
	/******************/
	static public function showResultsHTML($keyword, $catid, $articles, $total, $pageNav) {
		global $objArthrology, $Itemid; ?>
		
		<h2 class="arthrology_h2">
			<?php
			if ($keyword) {
				echo $objArthrology->lng->SRCH_RESULTS; 
				echo '"<i>'.$keyword.'</i>"';
			} else {
				if ($catid) {
					echo $objArthrology->lng->SRCH_RESULTS;
					echo '"<i>'.$articles[0]->cat_name.'</i>"';
				} else {
					echo $objArthrology->lng->SRCH_RESULTS_ALL;
				}
			}
			?>
		</h2>
		<br/>
		
			<table class="arthrology_printtable">
				<tr>
					<td class="left"><?php echo $objArthrology->lng->SRCH_RESULTS_FOUND1; ?><b><?php echo $total ?></b><?php echo $objArthrology->lng->SRCH_RESULTS_FOUND2; ?><br/><br/></td>
					<?php if ($total) { ?>
						<td class="right"><?php
							$image = mosAdminMenus::ImageCheck( 'printButton.png', '/images/M_images/', NULL, NULL, _CMN_PRINT, 'right' );
							$link = 'index2.php?option=com_arthrology&task=printlist&keyword='.$keyword.'&catid='.$catid;
							$status = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';
							
							if ( $objArthrology->task == 'printlist' ) {
								echo '<a href="javascript:void(0);" onclick="javascript:window.print(); return false" title="'._CMN_PRINT.'">'._LEND;
							} else {
								echo '<a href="javascript:void(0);" onclick="javascript:window.open(\''.$link.'\', \'PrintWindow\', \''.$status.'\');" title="'._CMN_PRINT.'">'._LEND;
							}
							echo $image._LEND;
							echo '</a> '._LEND;
						?></td>
					<?php } ?>
				</tr>
			</table>

		<?php
		if ($articles) {
			
			$k = 0;
			
			echo '<ul class="artrhology_ulist">'._LEND;
			for ($i = $pageNav->limitstart; $i < (count($articles) + $pageNav->limitstart); $i++) {
				$article = $articles[$i - $pageNav->limitstart];
				
				$out = '<li class="arthrology_row'.$k.'">'._LEND;
				$out .= '<span class="arthrology_bignum">'.($i+ 1).'</span>'._LEND;
				$out .= $article->title.' ['.$article->cat_name.' ';
				$out .= $article->year . '.' . $article->pages . ']' . _LEND;
				$out .= '<br/><span class="arthrology_ulistinfo">';
				$out .= '<b>' . $objArthrology->lng->GEN_AUTHOR . ':</b> ' . $article->author . ' - <b>Tags:</b> ' . $article->tags;
				$out .= '</span>';
				$out .= '</li>';
				
				echo $out;
				$k = 1 - $k;
			}
			echo '</ul><br />';

			if ($objArthrology->task != 'printlist') {
				$total_pages = ceil( $pageNav->total / $pageNav->limit );
				if ($total_pages > 1) { ?>
					<div class="sectiontablefooter">
						<?php 
						$link = 'index.php?option=com_arthrology&task=results&keyword='.$keyword.'&catid='.$catid.'&Itemid='.$Itemid;
						$seolink = ARTHBASE.'/results.html?keyword='.$keyword.'&catid='.$catid;
						echo $pageNav->writePagesLinks($link, $seolink);
						echo '<br />'._LEND;
						echo $pageNav->writePagesCounter();
						?>
					</div>
				<?php
				}
			}
		} else { ?>
			<div class="arthrology_searchbox" style="padding: 5px;">
				<?php echo $objArthrology->lng->SRCH_RESULTS_NONE; ?>
			</div><br />
			<?php
		}
		if ( $objArthrology->task != 'printlist' ) {
			clsArthrologyHTML::searchScreenHTML();
		}
	}

} ?>
