<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x+
 *
 * Backend HTML Event Handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html	
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Includes

/*****************************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S BACK-END HTML FUNCTIONALITY  */
/*****************************************************************************/
class clsArthrologyAdminHTML {

	// Initialize variables

	/**********************************************************************************/
	/*  Display control panel                                           CONTROL PANEL */
	/**********************************************************************************/ 
	static public function showControlPanelHTML() {
		global $objArthrology, $mainframe, $adminLanguage; // Pass global variables and objects

		// add custom CSS file
		?><script type="text/javascript">
			var objheader = document.getElementsByTagName("head")[0];
			var csslink = document.createElement("link");
			
			csslink.setAttribute("rel", "stylesheet");
			csslink.setAttribute("type", "text/css");
			csslink.setAttribute("href", "<?php echo $objArthrology->live_apath;?>/css/arthrology.admin.css");
			csslink.setAttribute("media", "all");
			
			objheader.appendChild(csslink);
		</script><?php
		
		// Load core HTML library
		mosCommonHTML::loadOverlib();
		?>

		<!-- An empty form, just to serve the toolbar -->
		<form action="index2.php" method="get" name="adminForm">
			<input type="hidden" name="option" value="com_arthrology" />
			<input type="hidden" name="task" value="" />
		</form>
		
		<!-- Screen header -->
		<table class="adminheading" width="100%">
			<tr>
				<th width="100%" style="background:url(<?php echo $objArthrology->live_apath; ?>/images/arthrology.png) no-repeat" style="text-align:left;">
					<?php echo $adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE; ?> <small>[<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP; ?>]</small>
				</th>
			</tr>
		</table>
		
		<table style="width: 100%;"><tr>
			<td style="vertical-align: top; width: 600px;">
				<div class="cp_option">
					<a href="index2.php?option=com_arthrology&amp;task=list">
						<img src="<?php echo $objArthrology->live_apath; ?>/images/arthrology_articles64.png" border="0" alt="<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_ARTICLES; ?>" />
					</a><br />
					<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_ARTICLES; ?>
				</div>
				<div class="cp_option">
					<a href="index2.php?option=com_categories&amp;section=com_arthrology">
						<img src="<?php echo $objArthrology->live_apath; ?>/images/arthrology_magazines64.png" border="0" alt="<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_MAGAZINES; ?>" />
					</a><br />
					<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_MAGAZINES; ?>
				</div>
				<div class="cp_option">
					<a href="index2.php?option=com_arthrology&task=conf&hidemainmenu=1">
						<img src="<?php echo $objArthrology->live_apath; ?>/images/arthrology_config64.png" border="0" alt="<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_CONFIG; ?>" />
					</a><br />
					<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_CONFIG; ?>
				</div>
			</td><td style="width: 5px;">
			</td><td style="color: #333333; font-size: 12px;">
				<h2><?php echo $adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE; ?> v1.3</h2>
				<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_DESCRIPTION; ?> <br />
				<br />
				<b><?php echo $adminLanguage->A_VERSION; ?>:</b> 1.3 <br/>
				<b><?php echo $adminLanguage->A_AUTHOR; ?>:</b> <?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_AUTHOR_NAME; ?> <br/>
				<b><?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_LICENSE; ?>:</b> GNU/GPL <br/>
				<br/>
				<b><?php echo $adminLanguage->A_CMP_ARTHROLOGY_CP_COMPATIBILITY; ?>:</b> Elxis CMS 2008.x and 2009.x <br/>
				<br/>
				Copyright © 2009-2011 Apostolos Koutsoulelos. All rights reserved. <br/>
				<br/>
			</td>
		</tr></table>
		<br/>
		<br/>
		<?php
	}

	/***********************************************************************************/
	/*  Show Configuration Panel										CONFIGURATION  */
	/***********************************************************************************/
	static public function showConfigurationHTML( $config_values ) {
		global $objArthrology, $adminLanguage; // Pass global variables and objects

		// Load core HTML library
		mosCommonHTML::loadOverlib(); ?>

		<script type="text/javascript">
			function submitbutton(task) {
				var form = document.adminForm;
				if (task == 'cp_cancel') {
					submitform( task );
					return;
				}

				if ( form.limit.value == "" ) {
					alert('<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_ALERT_LIMIT; ?>');
				} else {
					submitform(task);
				}
			}
			
			function checkImport() {
				var form = document.importForm;
				
				if ( form.cvs.value == "" ) {
					alert('<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_ALERT_NO_CVS; ?>');
					return false;
				} else if (form.catid.value == "0" ) {
					alert('<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_ALERT_NO_MAGAZINE; ?>');
					return false;
				} else {
					return true;
				}					
			}
		</script>

		<!-- Screen header -->
		<table class="adminheading" width="100%">
			<tr>
				<th width="100%" style="text-align:left;">
					<?php echo $adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE; ?> <small>[<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CONF; ?>]</small>
				</th>
			</tr>
		</table>

		<table class="adminform" style="height: 400px;">
			<tr>
				<td width="50%" valign="top">
					<?php 
						$tabulator = new mosTabs( 0 ); 
						$tabulator->startPane( "Configure" );
						$tabulator->startTab( $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_GENERAL, "general" );
					?>
					<form action="index2.php" method="post" name="adminForm">
						<input type="hidden" name="option" value="com_arthrology" />
						<input type="hidden" name="task" value="cp_save" />

						<table>
							<tr>
								<td valign="top">
									<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_LIMIT; ?>:
								</td>
								<td valign="top"><?php
									$length[] = mosHTML::makeOption( "15", "15" );
									$length[] = mosHTML::makeOption( "25", "25");
									$length[] = mosHTML::makeOption( "50", "50" );
									echo mosHTML::selectList($length, "limit", "","value", "text", $config_values->get('limit', '25')); ?>
								</td>
								<td align="right" valign="top">
									<?php echo mosToolTip( $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_LIMIT_TOOLTIP, $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_LIMIT ); ?>
								</td>
							</tr>
						</table>
					</form>
					<?php
						$tabulator->endTab();
						$tabulator->startTab( $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_IMPORT, "import" );
					?>
					<form action="index2.php" method="post" name="importForm">
						<input type="hidden" name="option" value="com_arthrology" />
						<input type="hidden" name="task" value="cp_import" />
						<table width="100%">
							<tr>
								<td valign="top" colspan="3">
									<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_TEXT; ?>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_MAGAZINE; ?>:
								</td>
								<td valign="top" colspan="2">
									<?php echo mosAdminMenus::ComponentCategory('catid','com_arthrology'); ?>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_CVS; ?>:
								</td>
								<td valign="top" colspan="2">
									<textarea id="cvs" name="cvs" cols="100" rows="40" style="width:100%px; height:300px;"></textarea>
								</td>
							</tr>
							<tr>
								<td><?php echo $adminLanguage->A_PUBLISHED ?>:</td>
								<td colspan="2">
									<?php echo mosHTML::yesnoRadioList( 'published', '', 1 ); ?>
								</td>
							</tr>
							<tr><td valign="top" colspan="3">
								<input class="input" type="submit" name="submit" value="<?php echo $adminLanguage->A_CMP_ARTHROLOGY_CONF_TAB_IMPORT; ?>" onclick="return checkImport()"/>
							</td></tr>
						</table>
					</form>
					<?php
						$tabulator->endTab();
						$tabulator->endPane();
					?>
				</td>
			</tr>
		</table>
	<?php
	}

	/**************************************************************************/
	/*  Display article list											EDIT  */
	/**************************************************************************/
	static public function listArticlesHTML($results, $pageNav, $catlist) {
		global $my, $adminLanguage, $objArthrology; // Pass global variables and objects 
		?>
		<form action="index2.php" method="get" name="adminForm">
		<input type="hidden" name="option" value="com_arthrology" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="field" value="<?php echo mosGetParam( $_REQUEST, "field", "") ?>" />
		<input type="hidden" name="order" value="<?php echo mosGetParam( $_REQUEST, "order", "none") ?>" />

	<script type="text/javascript" src="<?php echo $objArthrology->live_apath; ?>/arthrology.ajax.js"></script>

		<table class="adminheading" width="100%">
			<tr>
				<th rowspan="2" style="background: url(<?php echo $objArthrology->live_apath; ?>/images/arthrology.png) no-repeat;">
					<?php echo $adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE; ?> <span style="font-size: small;" dir="ltr"> [<?php echo $adminLanguage->A_CMP_ARTHROLOGY_LIST_ARTICLES ?>]</span>
				</th>

				<td align="right" valign="top"><?php echo $adminLanguage->A_FILTER ?>: &nbsp;</td>
				<td valign="top">
					<input name="search" value="<?php echo mosGetParam($_REQUEST, "search", "") ?>" class="inputbox" onchange="document.adminForm.submit();" type="text" />
				</td>
				<td align="right" valign="top">
					<?php echo $catlist; ?>
				</td>
			</tr>
		</table>

		<table class="adminlist">
			<tr>
				<th width="2%">#</th>
				<th width="2%"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($results); ?>);" /></th>
				<th width="56%" align="left"><?php echo clsArthrologyAdminHTML::sortIcon("title") ?><?php echo $adminLanguage->A_TITLE ?></th>
				<th width="5%"><nobr><?php echo clsArthrologyAdminHTML::sortIcon("published") ?><?php echo $adminLanguage->A_PUBLISHED ?></nobr></th>
				<th width="7%"><?php echo $adminLanguage->A_ID ?></th>
				<th width="20%" align="left"><nobr><?php echo clsArthrologyAdminHTML::sortIcon("catid") ?><?php echo $adminLanguage->A_CMP_ARTHROLOGY_LIST_MAGAZINE ?></nobr></th>
				<th width="10%"><?php echo clsArthrologyAdminHTML::sortIcon("year") ?><?php echo $adminLanguage->A_CMP_ARTHROLOGY_LIST_YEAR ?></th>
			</tr>
			<?php 
			$toggle = true; 
			$count = -1;
			$unpublished = 0;

			foreach ($results AS $art) { 
				$count++;
				$toggle = ($toggle)?false:true;
				$cat_params = new mosParameters ($art->cat_params);
				?>
				<tr class="<?php echo ($toggle)?"row0":"row1" ?>">
					<td><?php echo $pageNav->rowNumber( $count ); ?></td>
					<td><?php echo mosCommonHTML::CheckedOutProcessing( $art, $count ); ?></td>
					<td>
						<?php
						if ( $art->checked_out && ( $art->checked_out != $my->id ) ) {
							echo '<b>' . htmlspecialchars( $art->title, ENT_QUOTES ) . '</b>';
						} else {
						?>
							<a href="index2.php?option=com_arthrology&task=edit&hidemainmenu=1&cid=<?php echo $art->id ?>" title="<?php echo $adminLanguage->A_EDIT ?>">
								<?php echo htmlspecialchars($art->title, ENT_QUOTES); ?>
							</a>
							<?php
						}
						?>
					</td>
					<td align="center">
						<?php if ($art->published == 1) {
							$img = 'publish_g.png';
							$alt = $adminLanguage->A_PUBLISHED;
						} else if ($art->published == 0){
							$img = 'publish_x.png';
							$alt = $adminLanguage->A_UNPUBLISHED;
						} else if ($art->published == -1) {
							$img = 'publish_y.png';
							$alt = $adminLanguage->A_UNPUBLISHED;						
						} ?>
						<div id="constatus<?php echo $count; ?>">
						<a href="javascript: void(0);" onclick="changeContentState('<?php echo $count; ?>', '<?php echo $art->id; ?>', '<?php echo (($art->published == 0) || ($art->published == -1)) ? 1 : 0; ?>'); return nd();">
							<img src="images/<?php echo $img; ?>" width="12" height="12" border="0" alt="<?php echo $alt; ?>" />
						</a>
						</div>

					</td>
					<td align="center"><?php echo $art->id ?></td>
					<td align="left"><?php echo $art->cat_name ?></td>
					<td align="center"><?php echo $art->year ?></td>
				</tr>
			<?php
			}
			if ($unpublished) {
				?>
				<tr>
					<td colspan="10"></td>
				</tr>
			<?php
			}
			?>
		</table>
	<?php
		echo $pageNav->getListFooter();
	?>
	</form>
	<?php
	}

	/***********************************************************************************/
	/*  Display edit/add article										CONFIGURATION  */
	/***********************************************************************************/
	static public function editArticleHTML($article = null) {
		global $objArthrology, $adminLanguage, $database, $mosConfig_lifetime, $my;
		
        //CSRF prevention
        $tokname = 'token'.$my->id;
		$mytoken = md5(uniqid(rand(), TRUE));
        $_SESSION[$tokname] = $mytoken;
        
		mosCommonHTML::loadOverlib();
?>
		<script type="text/javascript">
			function submitbutton(pressbutton, section) {
				var form = document.adminForm;
				if (pressbutton == 'cancel') {
					submitform( pressbutton );
					return;
				}

				if ( form.title.value == "" ) {
					alert('<?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_ALERT_NO_TITLE ?>');
				} else if ( form.seotitle.value == "" ) {
					alert('<?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_ALERT_NO_SEOTITLE ?>');
				} else if ( form.catid.value == "0" ) {
					alert('<?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_ALERT_NO_MAGAZINE ?>');
				} else if ( form.author.value == "" ) {
					alert('<?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_ALERT_NO_AUTHOR ?>');
				} else if ( form.year.value == "" ) {
					alert('<?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_ALERT_NO_YEAR ?>');
				} else {
					submitform(pressbutton);
				}
			}

		</script>
		<!-- include AJAX scripts -->
		<script type="text/javascript" src="<?php echo $objArthrology->live_apath; ?>/arthrology.ajax.js"></script>

		<!-- include Countdown -->
		<div class="countdown">
			<?php echo $adminLanguage->A_TIMESESSEXP; ?>: <span id="countdown"></span>
		</div>

		<form action="index2.php" method="post" name="adminForm">
		<input type="hidden" name="option" value="com_arthrology" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="id" value="<?php echo (isset($article))?$article->id:""; ?>" />
		<input type="hidden" name="<?php echo $tokname; ?>" value="<?php echo $mytoken; ?>" autocomplete="off" />

			<table width="100%">
				<tr>
					<td colspan="2">
						<table class="adminheading">
							<tr>
								<th>
									<?php echo $adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE ?>:
									<?php 
									switch ( $objArthrology->task ) {
										case 'new':
											echo $adminLanguage->A_NEW;
											break;
										case 'edit':
											echo $adminLanguage->A_EDIT
												.' <span style="font-size: small;" dir="ltr">[ '
												.$adminLanguage->A_CATEGORY
												.': '
												.$article->category
												.']</span>';
											break;
									} ?>
								</th>
							</tr>
						</table>
					</td>
				</tr>
				<tr valign="top">
					<td width="60%">
						<table class="adminform">
							<tr>
								<th colspan="3" class="title"><?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_DETAILS ?></th>
							</tr>
							<tr>
								<td><?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_MAGAZINE; ?>:</td>
								<td>
									<?php
										if (isset($article)) {
											$active = $article->catid;
											$cat_params = new mosParameters($article->cat_params);
										} else {
											$active = NULL;
										}
										echo mosAdminMenus::ComponentCategory('catid','com_arthrology', $active);
									?>
									<?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_YEAR ?>: <input type="text" name="year" size="4" class="inputbox" value="<?php echo (isset($article))?htmlspecialchars( $article->year, ENT_QUOTES ):"" ?>" />
									<?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_PAGE ?>: <input type="text" name="pages" size="10" class="inputbox" value="<?php echo (isset($article))?htmlspecialchars( $article->pages, ENT_QUOTES ):"" ?>" />
								</td>
								<td></td>
							</tr>
							<tr>
								<td><?php echo $adminLanguage->A_TITLE; ?>:</td>
								<td><input type="text" name="title" size="60" class="inputbox" value="<?php echo (isset($article))?htmlspecialchars( $article->title, ENT_QUOTES ):"" ?>" /></td>
								<td></td>
							</tr>
							<tr>
								<td valign="top"><?php echo $adminLanguage->A_SEOTITLE; ?>:</td>
								<td>
									<input type="text" name="seotitle" id="seotitle" dir="ltr" class="inputbox" value="<?php echo (isset($article))?$article->seotitle:""; ?>" size="30" maxlength="100" /><br />
									<a href="javascript:;" onclick="suggestSEO()"><?php echo $adminLanguage->A_SEOTSUG; ?></a> &nbsp; | &nbsp; 
									<a href="javascript:;" onclick="validateSEO()"><?php echo $adminLanguage->A_SEOTVAL; ?></a><br />
									<div id="valseo" style="height: 20px;"></div>                              
								</td>
								<td>
									<?php echo mosToolTip($adminLanguage->A_SEOTHELP, $adminLanguage->A_SEOTITLE); ?>
								</td>
							</tr>
							<tr>
								<td><?php echo $adminLanguage->A_CMP_ARTHROLOGY_EDIT_AUTHOR; ?>:</td>
								<td><input type="text" name="author" size="60" class="inputbox" value="<?php echo (isset($article))?htmlspecialchars( $article->author, ENT_QUOTES ):"" ?>" /></td>
							</tr>
							<tr>
								<td><?php echo $adminLanguage->A_PUBLISHED ?>:</td>
								<td colspan="2">
									<?php echo mosHTML::yesnoRadioList( 'published', '', intval( isset($article) && $article->published ) ); ?>
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo $adminLanguage->A_DESCRIPTION ?>:</td>
								<td colspan="2">
									<?php
									//parameters : areaname, content, hidden field, width, height, rows, cols
									editorArea( 'editor1',  ((isset($article))?$article->description:'') , 'description', '450', '300', '60', '20' );
									?>
								</td>
							</tr>
						</table>
					</td>
					<td width="40%">
						<?php 
							$tabulator = new mosTabs( 0 ); 
							$tabulator->startPane( "Configure" );
							$tabulator->startTab( $adminLanguage->A_CMP_ARTHROLOGY_EDIT_TAGS, "repeat" );
						?>
						<table class="adminform">
							<tr valign="top">
								<td colspan="2"><input type="text" name="tags" size="60" class="inputbox" value="<?php echo (isset($article))?htmlspecialchars( $article->tags, ENT_QUOTES ):"" ?>" /></td>
								<td>
									<?php echo mosToolTip($adminLanguage->A_CMP_ARTHROLOGY_EDIT_TAGS_TOOLTIP, $adminLanguage->A_CMP_ARTHROLOGY_EDIT_TAGS) ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
		
		<script type="text/javascript">
			//start session countdown
			function sessioncountdown(secs) {
				var cel = document.getElementById('countdown');
				if (secs > 0) {
					var dmins = Math.ceil(secs/60);
					var text = '';
					if (dmins > 1) {
						text = '<strong>'+dmins+'</strong> <?php echo $adminLanguage->A_MINUTES; ?>';
					} else if (secs == 60) {
						text = '<strong>1</strong> <?php echo $adminLanguage->A_MINUTE; ?>';
					} else if (secs > 30) {
						text = '<strong>'+secs+'</strong> <?php echo $adminLanguage->A_SECONDS; ?>';
					} else if (secs > 1) {
						text = '<span style="color:red;"><strong>'+secs+'</strong> <?php echo $adminLanguage->A_SECONDS; ?></span>';
					} else if (secs == 1) {
						text = '<span style="color:red;"><strong>1</strong> <?php echo $adminLanguage->A_SECOND; ?></span>';
					}
					cel.innerHTML = text;
					secs = secs -1;
					setTimeout("sessioncountdown("+secs+")",1000);				
				} else {
					cel.innerHTML = '<span style="color: red; font-weight: bold;"><?php echo $adminLanguage->A_SESSEXPIRED; ?></span>';
				}
			}

			sessioncountdown('<?php echo $mosConfig_lifetime; ?>');
		</script>

	<?php }
	
	/****************************************************************************/
	/*  Return the the source-code for a sort-icon						HELPER  */
	/****************************************************************************/
	function sortIcon( $field ) {
		$valid_array = Array("option", "field", "state", "published", "catid");
		
		if ( mosGetParam( $_REQUEST, "field", "") == $field)
			$state = mosGetParam( $_REQUEST, "order", NULL);
		
		$params = array_keys( $_REQUEST );
		$base = "";

		for ( $i=0; $i < count( $_REQUEST ); $i++ ) {
			if ( in_array($params[$i], $valid_array) ) {
				$base = $base . "&" . $params[$i] . "=" . $_REQUEST[$params[$i]];
			}
		}

		$base = "index2.php?" . substr( $base, 1 );

		if (isset($state)) {
			return mosHTML::sortIcon( $base, $field, $state ) . "&nbsp;";
		} else {
			return mosHTML::sortIcon( $base, $field ) . "&nbsp;";
		}
	}

} ?>
