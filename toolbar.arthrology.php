<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * Backend Toolbar Event Handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html			
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Includes
require_once($mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/toolbar.arthrology.html.php'); // Component's html file for the administration area

// Intialize variables

// Display toolbar
switch ($task) {
	case 'edit':
	case 'new':
		clsArthrologyToolbarHTML::_EDIT();
		break;
	case 'list':
		clsArthrologyToolbarHTML::_LIST();
		break;
	case 'conf':
		clsArthrologyToolbarHTML::_CONF();
		break;
	case 'cp':
	default:
		clsArthrologyToolbarHTML::_CP();
		break;
}
?>
