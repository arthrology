<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * Greek Language File (backend area)
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html	
 * 
 * Special thanx to Duilio Lupini (Speck - info@elxisitalia.com) for 
 * grammar and spelling checks!
 * 
 */
 
// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );


class adminLanguage extends standardLanguage {

	// Set translation variables
	public $A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE = 'Αρθρολόγιο';

	/*************/
	/*  TOOLBAR  */
	/*************/
	public $A_CMP_ARTHROLOGY_TOOLBAR_CONFIG = 'Ρυθμίσεις';
	public $A_CMP_ARTHROLOGY_TOOLBAR_ARTICLES = 'Άρθρα';
	public $A_CMP_ARTHROLOGY_TOOLBAR_MAGAZINES = 'Περιοδικά';

	/*******************/
	/*  CONTROL PANEL  */
	/*******************/
	public $A_CMP_ARTHROLOGY_CP = 'Πίνακας ελέγχου';
	public $A_CMP_ARTHROLOGY_CP_ARTICLES = 'Διαχείριση άρθρων';
	public $A_CMP_ARTHROLOGY_CP_MAGAZINES = 'Διαχείριση περιοδικών';
	public $A_CMP_ARTHROLOGY_CP_CONFIG = 'Ρυθμίσεις';
	public $A_CMP_ARTHROLOGY_CP_DESCRIPTION = '<p>Το <strong>Arthrology</strong> είναι ένα  σύστημα διαχείρισης άρθρων για το Elxis CMS 2008.x and 2009.x. Σας επιτρέπει να αποθηκεύεται και ευρετηριάζετε άρθρα δημοσιευμένα σε περιοδικά. Δημιουργήθηκε από τον Απόστολο Κουτσουλέλο (<a href="http://www.bitcraft-labs.gr" target="_blank" title="Open-source software and hardware">bitcraft-labs.gr</a>), και  διανέμεται υπό την <em>GNU/GPL</em> άδεια <em>δωρεάν</em>.<br /><br /><strong>Χαρακτηριστικά:</strong><br />* Αποθηκεύεστε οποιοαδήποτε άρθρο από περιοδικό.<br />* SEO PRO όνομα βάσης <em>arthrology</em> για το Elxis 2009.x<br />* Εισάγετε άρθα από αρχεία CVS.<br />* Αναζήτηση.<br />* Εκτυπώσιμα αποτελέσματα αναζήτησης.<br />* Επέκταση για το IOS Sitemap.<br />* RSS feeds με τα τελευταία άρθρα.<br /><br /><strong>Οδηγίες:</strong><br />Παρακαλώ επισκεφθείτε: <a href="http://wiki.elxis.org" target="_blank">Elxis Wiki</a>::<a href="http://wiki.elxis.org/wiki/Arthrology_(component)" target="_blank">Arthrology (component)</a>';
	public $A_CMP_ARTHROLOGY_CP_AUTHOR_NAME = 'Απόστολος Κουτσουλέλος';
	public $A_CMP_ARTHROLOGY_CP_LICENSE = 'Άδεια';
	public $A_CMP_ARTHROLOGY_CP_COMPATIBILITY = 'Συμβατότητα';
	
	/*******************/
	/*  CONFIGURATION  */
	/*******************/
	public $A_CMP_ARTHROLOGY_CONF = 'Ρυθμίσεις';
	public $A_CMP_ARTHROLOGY_CONF_TAB_GENERAL = 'Γενικά';
	public $A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_LIMIT = 'Όριο';
	public $A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_LIMIT_TOOLTIP = 'Ορίστε πόσα άρθρα θα εμφανίζονται σε κάθε σελίδα.';
	public $A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_ALERT_NO_LIMIT = 'Πρέπει να ορίσετε το όριο!';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT = 'Εισαγωγή';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_TEXT = 'Παρακαλώ εισάγατε τα άρθρα σε μορφή CVS (Συγγραφέας,,Τίτλος,,Σελίδες,,Έτος).';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_MAGAZINE = 'Περιοδικό';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_CVS = 'CVS';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_ALERT_NO_CVS = 'Πρέπει να εισάγετε τα άρθρα σε μορφή CVS!';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_ALERT_NO_MAGAZINE = 'Πρέπει να εισάγετε περιοδικό!';
	public $A_CMP_ARTHROLOGY_CONF_ALERT_NO_CVS = 'Πρέπει να εισάγετε τα άρθρα σε μορφή CVS!';
	public $A_CMP_ARTHROLOGY_CONF_ALERT_NO_MAGAZINE = 'Πρέπει να εισάγετε περιοδικό!';
	public $A_CMP_ARTHROLOGY_CONF_ALERT_SUCCESS = ' άρθρα εισήχθησαν επιτυχώς!';
	public $A_CMP_ARTHROLOGY_CONF_ALERT_FAIL = 'Αποτυχία εισαγωγής άρθρων!';
	public $A_CMP_ARTHROLOGY_CONF_MSG_SUCCESS = 'Οι ρυθμίσεις αποθηκεύθηκαν!';
	public $A_CMP_ARTHROLOGY_CONF_MSG_FAIL = 'Αποτυχία αποθήκευσης ρυθμίσεων!';

	/*******************/
	/*  LIST ARTICLES  */
	/*******************/
	public $A_CMP_ARTHROLOGY_LIST_ALL_ARTICLES = 'Όλα τα άρθρα';
	public $A_CMP_ARTHROLOGY_LIST_OLD_ARTICLES = 'Παλαια άρθρα';
	public $A_CMP_ARTHROLOGY_LIST_ARTICLES = 'Άρθρα';
	public $A_CMP_ARTHROLOGY_LIST_MAGZINE = 'Περιοδικό';
	public $A_CMP_ARTHROLOGY_LIST_YEAR = 'Έτος';

	/*******************/
	/*  EDIT ARTICLES  */
	/*******************/
	public $A_CMP_ARTHROLOGY_EDIT_MSG_SUCCESS = 'Το άρθρο αποθηκεύθηκε επιτυχώς!';
	public $A_CMP_ARTHROLOGY_EDIT_MSG_FAIL = 'Αποτυχία αποθήκευσης του άρθρου!';
	public $A_CMP_ARTHROLOGY_EDIT_DETAILS = 'Λεπτομέριες';
	public $A_CMP_ARTHROLOGY_EDIT_MAGAZINE = 'Περιοδικό';
	public $A_CMP_ARTHROLOGY_EDIT_YEAR = 'Έτος';
	public $A_CMP_ARTHROLOGY_EDIT_PAGE = 'Σελίδα';
	public $A_CMP_ARTHROLOGY_EDIT_AUTHOR = 'Συγγραφέας';
	public $A_CMP_ARTHROLOGY_EDIT_TAGS = 'Ετικέτες';
	public $A_CMP_ARTHROLOGY_EDIT_TAGS_TOOLTIP = "Παρακαλώ εισάγατε ετικέτες για το άρθρο, διαχωρισμένες με κόμμα ','";
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_TITLE = 'Πρέπει να εισάγετε τίτλο για το άρθρο!';
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_SEOTITLE = 'Πρέπει να εισάγετε τίτλο SEO για το άρθρο!';
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_MAGAZINE = 'Πρέπει να εισάγετε περιοδικό για το άρθρο!';
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_AUTHOR = 'Πρέπει να εισάγετε συγγραφέα για το άρθρο!';
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_YEAR = 'Πρέπει να εισάγετε έτος δημοσίευσης για το άρθρο!';

	// Just an empty constructor
	public function __construct() {
	}
}
?>
