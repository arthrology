<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * English Language File (front-end)
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html	
 * 
 * Special thanx to Duilio Lupini (Speck - info@elxisitalia.com) for 
 * grammar and spelling checks!
 * 
 */
 
// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class clsArthrologyLng {
	
	// Set translation variables
	public $GEN_COMPONENT_TITLE = 'Arthrology';
	
	/******************/
	/*  INSTALLATION  */
	/******************/
	public $INS_HEADER = 'Installation of Arthrology <small><small>by Apostolos Koutsoulelos</small></small>';
	public $INS_ERROR_MENU_PARAMS = 'Error updating database. Unable to store component default parameters.';
	public $INS_ERROR_MENU_MAIN = 'Error updating database. Backend Menu Items (Arthrology) not updated.<br/>';
	public $INS_ERROR_MENU_CP = 'Error updating database. Backend Menu Items (Control Panel) not updated.<br/>';
	public $INS_ERROR_MENU_ART = 'Error updating database. Backend Menu Items (Manage Articles) not updated.<br/>';
	public $INS_ERROR_MENU_MAG = 'Error updating database. Backend Menu Items (Manage Categories) not updated.<br/>';
	public $INS_ERROR_MENU_CONF = 'Error updating database. Backend Menu Items (Configuration) not updated.<br/>';
	public $INS_ERROR_SEOPRO = 'Error coping SEO PRO extension to folder includes/seopro/. Please copy and rename it manually!<br/>';
	public $INS_ERROR_SITEMAP = 'Error coping IOS Sitemap extension to folder admnistrator/components/com_sitemap/extensions/. Please copy it manually!<br/>';
	public $INS_ERROR_TOOLBAR = 'Error coping toolbar icons to folder administrator/images/. Please copy them manually!<br/>';
	public $INS_ERROR_NOTICE_TITLE = 'Installation notices';
	public $INS_ERROR_NOTICE = 'All the above errors are very small errors, mainly making the user interface of Arthrology a little bit more friendly. You do not need to worry about them!';
	public $INS_CRITICAL_LNG_ENGLISH = 'Error updating administration laguage (english). Please copy /administrator/components/com_arthology/language/english.com_arthology.php in folder /administrator/language/english manually.';
	public $INS_CRITICAL_LNG_GREEK = 'Error updating administration laguage (greek). Please copy /administrator/components/com_arthology/language/greek.com_arthology.php in folder /administrator/language/greek manually.';
	public $INS_CRITICAL_LNG_ITALIAN = 'Error updating administration laguage (italian). Please copy /administrator/components/com_arthology/language/italian.com_arthology.php in folder /administrator/language/italian manually.';
	public $INS_CRITICAL_NOTICE_TITLE = 'Critical errors';
	public $INS_CRITICAL_NOTICE = 'All the errors above are critical. Please follow the instructions above to fix them!';
	public $INS_TITLE = 'Arthrology - An event management system Elxis CMS 2008.x and 2009.x+';
	public $INS_BODY = '<br/><b>Arthrology was successfully installed</b><br/><br/>Arthrology is a storing and indexing system for atricles published in magazines.<br/><br/>';
	
	/**********/
	/*  HTML  */
	/**********/
	public $ALERT_HTML_NO_KEY = 'You must set al least one search keyword!';
	public $SRCH_KEYW_ENTER = 'Enter one keyword for search (al least 3 cahracters long). Leave empty for all articles in selected magazine.';
	public $SRCH_MAG = 'Magazine';
	public $SRCH_KEYW = 'Keyword';
	public $SRCH_RESULTS = 'Results for ';
	public $SRCH_RESULTS_ALL = 'All articles';
	public $SRCH_RESULTS_FOUND1 = 'Total ';
	public $SRCH_RESULTS_FOUND2 = ' articles were found.';
	public $SRCH_RESULTS_NONE = ' No results found...';

	// Just an empty constructor
	public function __construct() {
	}
}
?>
