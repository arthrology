<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * Greek Language File (front-end)
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html	
 */
 
// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class clsArthrologyLng {
	
	// Set translation variables
	public $GEN_COMPONENT_TITLE = 'Αρθρολόγιο';
	
	/******************/
	/*  INSTALLATION  */
	/******************/
	public $INS_HEADER = 'Εγκατάσταση της Αρθρολογίας <small><small>από τον Απόστολο Κουτσουλέλο</small></small>';
	public $INS_ERROR_MENU_PARAMS = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Δεν στάθηκε δυνατή η αποθήκευση των προκαθορισμένων παραμέτρων του component.';
	public $INS_ERROR_MENU_MAIN = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Αρθρολόγιο] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_MENU_CP = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Κέντρο Ελέγχου] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_MENU_ART = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Διαχείριση Άρθρων] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_MENU_MAG = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Διαχείριση Περιοδικών] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_MENU_CONF = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Ρυθμίσεις] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_SEOPRO = 'Σφάλμα κατά την αντιγραφή της SEO PRO επέκτασης στον φάκελο includes/seopro/. Παρακαλώ αντιγράψτε το και μετονομάστε το χειροκίνητα!<br/>';
	public $INS_ERROR_SITEMAP = 'Σφάλμα κατά την αντιγραφή της επέκτασης του IOS Sitemap στον φάκελο admnistrator/components/com_sitemap/extensions/. Παρακαλώ αντιγράψτε το χειροκίνητα!<br/>';
	public $INS_ERROR_TOOLBAR = 'Σφάλμα κατά την αντιγραφή των εικονοδίων της εργαλειοθήκης στον φάκελο administrator/images/. Παρακαλώ αντιγράψτε τα χειροκίνητα!<br/>';
	public $INS_ERROR_NOTICE_TITLE = 'Παρατηρήσεις εγκατάστασης';
	public $INS_ERROR_NOTICE = 'Τα παραπάνω σφάλματα είναι δευτερεύοντα, κυρίως κάνουν το περιβάλλον χρήστη του Αρθρολογίου φιλικότερο. Δεν χρειάζεται να ανησυχείτε για αυτά!';
	public $INS_CRITICAL_LNG_ENGLISH = 'Σφάλμα κατά την ενημέρωση της γλώσσας διαχείρισης (αγγλικά). Παρακαλώ αντιγράψτε το αρχείο /administrator/components/com_arthrology/language/english.com_arthrology.php στον φάκελο /administrator/language/english χειροκίνητα.';
	public $INS_CRITICAL_LNG_GREEK = 'Σφάλμα κατά την ενημέρωση της γλώσσας διαχείρισης (ελληνικά). Παρακαλώ αντιγράψτε το αρχείο /administrator/components/com_arthrology/language/greek.com_arthrology.php στον φάκελο /administrator/language/greek χειροκίνητα.';
	public $INS_CRITICAL_LNG_ITALIAN = 'Σφάλμα κατά την ενημέρωση της γλώσσας διαχείρισης (ιταλικά). Παρακαλώ αντιγράψτε το αρχείο /administrator/components/com_arthrology/language/greek.com_arthrology.php στον φάκελο /administrator/language/italian χειροκίνητα.';
	public $INS_CRITICAL_NOTICE_TITLE = 'Κρίσιμα σφάλματα';
	public $INS_CRITICAL_NOTICE = 'Τα παραπάνω σφάλματα είναι σημαντικά και επιρρεάζουν την λειτουργικότητα της επέκτασης. Παρακαλώ ακολουθείστε τις παραπάνω οδηγίες για την διόρθωσή τους!';
	public $INS_TITLE = 'Αρθρολόγιο - Σύστημα διαχείρισης άρθρων για το Elxis CMS 2008.x και 2009.x+';
	public $INS_BODY = '<br/><b>Το Αρθρολόγιο εγκαταστάθηκε επιτυχώς</b><br/><br/>Το Αρθρολόγιο είναι ένα σύστημα καταχώρησης και ευρετηρίασης άρθρων δημοσιευμένων σε περιοδικά.<br/><br/>';

	/**********/
	/*  HTML  */
	/**********/
	public $ALERT_HTML_NO_KEY = 'Πρέπει να ορίσετε μία τουλάχιστον λέξι κλειδί για την έρευνα!';
	public $SRCH_KEYW_ENTER = 'Εισάγετε μία λέξη κλειδί προς αναζήτηση (ελάχιστο μήκος 3 χαρακτήρες). Αφήστε κενό για σύνολο άρθρων επιλεγμένου περιοδικού.';
	public $SRCH_KEYW = 'Λέξη κλειδί';
	public $SRCH_MAG = 'Περιοδικό';
	public $SRCH_RESULTS = 'Αποτελέσματα για το ';
	public $SRCH_RESULTS_ALL = 'Σύνολο άρθρων';
	public $SRCH_RESULTS_FOUND1 = 'Βρέθηκαν ';
	public $SRCH_RESULTS_FOUND2 = ' άρθρα.';
	public $SRCH_RESULTS_NONE = ' Δεν βρέθηκαν αποτελέσματα...';
	
	// Just an empty constructor
	public function __construct() {
	}
}
?>
