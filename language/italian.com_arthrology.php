<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * Italian Language File (backend area)
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html	
 * 
 * Special thanx to Duilio Lupini (Speck - info@elxisitalia.com) for 
 * the translation!
 * 
 */
 
// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );


class adminLanguage extends standardLanguage {

	// Set translation variables
	public $A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE = 'Arthrology';

	/*************/
	/*  TOOLBAR  */
	/*************/
	public $A_CMP_ARTHROLOGY_TOOLBAR_CONFIG = 'Configuration';
	public $A_CMP_ARTHROLOGY_TOOLBAR_ARTICLES = 'Articles';
	public $A_CMP_ARTHROLOGY_TOOLBAR_MAGAZINES = 'Magazines';

	/*******************/
	/*  CONTROL PANEL  */
	/*******************/
	public $A_CMP_ARTHROLOGY_CP = 'Control Panel';
	public $A_CMP_ARTHROLOGY_CP_ARTICLES = 'Manage articles';
	public $A_CMP_ARTHROLOGY_CP_MAGAZINES = 'Manage magazines';
	public $A_CMP_ARTHROLOGY_CP_CONFIG = 'Configuration';
	public $A_CMP_ARTHROLOGY_CP_DESCRIPTION = '<p><strong>Arthrology</strong> is an article manager component for Elxis CMS 2008.x and 2009.x. It lets you store and index articles published in magazines. Created by Apostolos Koutsoulelos (<a href="http://www.bitcraft-labs.gr" target="_blank" title="Open-source software and hardware">bitcraft-labs.gr</a>), released under the <em>GNU/GPL</em> license for <em>free</em>.<br /><br /><strong>Features:</strong><br />* Store any article from any magazine.<br />* SEO PRO base name <em>arthrology</em> for Elxis 2009+<br />* Import articles from CVS files.<br />* Search option.<br />* Printable search results.<br />* IOS Sitemap extension.<br />* RSS feeds for latest articles.<br /><br /><strong>Instructions:</strong><br />Please visit: <a href="http://wiki.elxis.org" target="_blank">Elxis Wiki</a>::<a href="http://wiki.elxis.org/wiki/Arthrology_(component)" target="_blank">Arthrology (component)</a>';
	public $A_CMP_ARTHROLOGY_CP_AUTHOR_NAME = 'Apostolos Koutsoulelos';
	public $A_CMP_ARTHROLOGY_CP_LICENSE = 'License';
	public $A_CMP_ARTHROLOGY_CP_COMPATIBILITY = 'Compatibility';
	
	/*******************/
	/*  CONFIGURATION  */
	/*******************/
	public $A_CMP_ARTHROLOGY_CONF = 'Configuration';
	public $A_CMP_ARTHROLOGY_CONF_TAB_GENERAL = 'General';
	public $A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_LIMIT = 'Limit';
	public $A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_LIMIT_TOOLTIP = 'Set how many results will be displayed in each page.';
	public $A_CMP_ARTHROLOGY_CONF_TAB_GENERAL_ALERT_NO_LIMIT = 'You must select the list limit!';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT = 'Import';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_TEXT = 'Please enter articles in CVS form (Author,,Title,,Pages,,Year).';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_MAGAZINE = 'Magazine';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_CVS = 'CVS';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_ALERT_NO_CVS = 'You must enter articles in CVS form!';
	public $A_CMP_ARTHROLOGY_CONF_TAB_IMPORT_ALERT_NO_MAGAZINE = 'You must enter the magazine!';
	public $A_CMP_ARTHROLOGY_CONF_ALERT_NO_CVS = 'You must enter articles in CVS form!';
	public $A_CMP_ARTHROLOGY_CONF_ALERT_NO_MAGAZINE = 'You must select a magazine for this article!';
	public $A_CMP_ARTHROLOGY_CONF_ALERT_SUCCESS = ' articles have been successfully stored!';
	public $A_CMP_ARTHROLOGY_CONF_ALERT_FAIL = 'Failed to store articles!';
	public $A_CMP_ARTHROLOGY_CONF_MSG_SUCCESS = 'Configuration stored!';
	public $A_CMP_ARTHROLOGY_CONF_MSG_FAIL = 'Failed to store configuration!';

	/*******************/
	/*  LIST ARTICLES  */
	/*******************/
	public $A_CMP_ARTHROLOGY_LIST_ALL_ARTICLES = 'All articles';
	public $A_CMP_ARTHROLOGY_LIST_OLD_ARTICLES = 'Old articles';
	public $A_CMP_ARTHROLOGY_LIST_ARTICLES = 'Articles';
	public $A_CMP_ARTHROLOGY_LIST_MAGZINE = 'Magazine';
	public $A_CMP_ARTHROLOGY_LIST_YEAR = 'Year';

	/*******************/
	/*  EDIT ARTICLES  */
	/*******************/
	public $A_CMP_ARTHROLOGY_EDIT_MSG_SUCCESS = 'Article have been successfully stored!';
	public $A_CMP_ARTHROLOGY_EDIT_MSG_FAIL = 'Failed to store article!';
	public $A_CMP_ARTHROLOGY_EDIT_DETAILS = 'Details';
	public $A_CMP_ARTHROLOGY_EDIT_MAGAZINE = 'Magazine';
	public $A_CMP_ARTHROLOGY_EDIT_YEAR = 'Year';
	public $A_CMP_ARTHROLOGY_EDIT_PAGE = 'Page';
	public $A_CMP_ARTHROLOGY_EDIT_AUTHOR = 'Author';
	public $A_CMP_ARTHROLOGY_EDIT_TAGS = 'Tags';
	public $A_CMP_ARTHROLOGY_EDIT_TAGS_TOOLTIP = "Please set the tags for this article, seperated by comma ','";
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_TITLE = 'You must set a title for this article!';
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_SEOTITLE = 'You must set a SEO title for this article!';
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_MAGAZINE = 'You must select a magazine for this article!';
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_AUTHOR = 'You must set an auhtor for this article!';
	public $A_CMP_ARTHROLOGY_EDIT_ALERT_NO_YEAR = 'You must set a publishing year for this article!';

	// Just an empty constructor
	public function __construct() {
	}
}
?>
