<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * Italian Language File (front-end)
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html	
 *
 * Special thanx to Duilio Lupini (Speck - info@elxisitalia.com)
 * 
 */
 
// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class clsArthrologyLng {
	
	// Set translation variables
	public $GEN_COMPONENT_TITLE = 'Arthrology';

	/******************/
	/*  INSTALLATION  */
	/******************/
	public $INS_HEADER = 'Installazione di Arthrology <small><small>di Apostolos Koutsoulelos</small></small>';
	public $INS_ERROR_MENU_PARAMS = 'Errore aggiornamento database. Non possibile Error updating database. Non possibile inserire i parametri di default del componente.';
	public $INS_ERROR_MENU_CP = 'Error updating database. Backend Menu Items (Control Panel) not updated.<br/>';																// TRANSLATION NEEDED
	public $INS_ERROR_MENU_MAIN = 'Error updating database. Backend Menu Items (Arthrology) not updated.<br/>';																	// TRANSLATION NEEDED
	public $INS_ERROR_MENU_ART = 'Errore aggiornamento database. Backend Voci Menù (Gestione Articoli) non aggiornato.<br/>';
	public $INS_ERROR_MENU_MAG = 'Errore aggiornamento database. Backend Voci Menù (Gestione Categorie) non aggiornato.<br/>';
	public $INS_ERROR_MENU_CONF = 'Errore aggiornamento database. Backend Voci Menù (Configurazione) non aggiornato.<br/>';
	public $INS_ERROR_SEOPRO = 'Erroe di copiatura della estensione SEO PRO nella cartella includes/seopro/. Per favove effettuare la copia e rinominarla manualmente!<br/>';
	public $INS_ERROR_SITEMAP = 'Error coping IOS Sitemap extension to folder admnistrator/components/com_sitemap/extensions/. Please copy it manually!<br/>';					// TRANSLATION NEEDED
	public $INS_ERROR_TOOLBAR = 'Error coping toolbar icons to folder administrator/images/. Please copy them manually!<br/>';													// TRANSLATION NEEDED
	public $INS_ERROR_NOTICE_TITLE = 'Insttallazione Notizie';
	public $INS_ERROR_NOTICE = 'Tutti gli errori segnalati sopra sono piccoli errori, causati principalmente nel fare l\'interfaccia del Arthrology più amichevole. Non dovete preccuparvi di questi errori!';							// TRANSLATION NEEDED
	public $INS_CRITICAL_LNG_ENGLISH = 'Error updating administration laguage (english). Please copy /administrator/components/com_arthology/language/english.com_arthology.php in folder /administrator/language/english manually.';	// TRANSLATION NEEDED
	public $INS_CRITICAL_LNG_GREEK = 'Error updating administration laguage (greek). Please copy /administrator/components/com_arthology/language/greek.com_arthology.php in folder /administrator/language/greek manually.';			// TRANSLATION NEEDED
	public $INS_CRITICAL_LNG_ITALIAN = 'Error updating administration laguage (italian). Please copy /administrator/components/com_arthology/language/italian.com_arthology.php in folder /administrator/language/italian manually.';	// TRANSLATION NEEDED
	public $INS_CRITICAL_NOTICE_TITLE = 'Critical errors';																														// TRANSLATION NEEDED
	public $INS_CRITICAL_NOTICE = 'All the errors above are critical. Please follow the instructions above to fix them!';														// TRANSLATION NEEDED
	public $INS_TITLE = 'Arthrology - Un Gestionale Eventi per Elxis CMS 2008.x and 2009.x+';
	public $INS_BODY = '<br/><b>Arthrology was successfully installed</b><br/><br/>Arthrology is a storing and indexing system for atricles published in magazines.<br/><br/>';	// TRANSLATION NEEDED

	/**********/
	/*  HTML  */
	/**********/
	public $ALERT_HTML_NO_KEY = 'Settare almeno una parola chiave!';
	public $SRCH_KEYW_ENTER = 'Inserire una parola chiave per la ricerca (lunghezza minima 3 caratteri). Lasciare vuoto per vedere tutti gli articoli nella rivista selezionata.';
	public $SRCH_KEYW = 'Parola Chiave';
	public $SRCH_MAG = 'Magazine';			// TRANSLATION NEEDED
	public $SRCH_RESULTS = 'Risultati per ';
	public $SRCH_RESULTS_ALL = 'Tutti gli Articoli';
	public $SRCH_RESULTS_FOUND1 = 'Totale ';
	public $SRCH_RESULTS_FOUND2 = ' articoli trovati.';
	public $SRCH_RESULTS_NONE = ' Nessun risultato trovato...';

	// Just an empty constructor
	public function __construct() {
	}
}
?>
