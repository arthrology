<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Sitemap extension
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

if (!defined('EVCALBASE')) {
	global $_VERSION;
	if ($_VERSION->RELEASE > 2008) {
		define('EVCALBASE', 'events');
	} else {
		define('EVCALBASE', 'com_eventcalendar');
	}
}

class sitemap_arthrology {

	/***************/
	/* CONSTRUCTOR */
	/***************/
	public function __construct() {

	}

	/****************************/
	/* MENU ITEM TYPE VALIDATOR */
	/****************************/
	public function isOfType( $parent ) {
		if (strpos($parent->link, 'option=com_arthrology')) { return true; }
		return false;
	}


	/********************/
	/* RETURN NODE TREE */
	/********************/
	public function getTree($parent) {
		global $database, $xartis, $mainframe, $my;

		$query = "SELECT id FROM #__menu"
		."\n WHERE link = 'index.php?option=com_arthrology' AND published = '1'"
		."\n AND ((language IS NULL) OR (language LIKE '%".$xartis->maplang."%'))";
		$database->setQuery($query, '#__', 1, 0);
		$_Itemid = intval($database->loadResult());

		$database->setQuery("SELECT * FROM #__categories WHERE section='com_arthrology' ORDER BY title ASC");
		$rows = $database->loadObjectList();

		$list = array();
		if ($rows) {
			foreach ($rows as $row) {
				$node = new stdClass;
				$node->name = $row->title;
				$node->link = 'index.php?option=com_arthrology&amp;task=magview&amp;catid='.$row->id;
				$node->seolink = EVCALBASE.'/'.$row->seotitle.'/';
				$node->id = $_Itemid;
				$node->modified = time() + ($mainframe->getCfg('offset') * 3600);
				$node->changefreq = 'daily';
				$node->priority = '0.8';
				$node->pid = 0;
				$node->icon = 'category';
		    	$list[$row->id] = $node;
		    	unset($node);
			}
		}

		return $list;
	}

}


global $xartis;
$tmp = new sitemap_arthrology;
$xartis->addExtension($tmp);
unset($tmp);

?>
