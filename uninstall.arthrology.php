<?php

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * Uninstall Handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Function automatically triggered by Elxis to fire the uninstall process 
function com_uninstall() {
    global $database, $fmanager, $mainframe;

	// Drop components table
	$tbl = $database->_table_prefix.'arthrology';
	if (in_array($database->_resource->databaseType, array('oci8', 'oci805', 'oci8po', 'oracle'))) { $tbl = strtoupper($tpl); }
	$dict = NewDataDictionary($database->_resource);
	$sql = $dict->DropTableSQL($tbl);
	$database->_resource->Execute($sql[0]);

	// Clear components categories
	$database->setQuery("DELETE FROM #__categories WHERE section='com_arthrology'");
	$database->query();

    // Remove the SEO PRO extension from the includes/seopro/ directory
    $seofile = $mainframe->getCfg('absolute_path').'/includes/seopro/com_arthrology.php';
    if (file_exists($seofile)) { $fmanager->deleteFile($seofile); }
    
    //Remove the IOS Sitemap extension from the IOS Sitemap extensions directory
    $sitemapfile = $mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/extensions/arthrology.sitemap.php';
    if (file_exists($sitemapfile)) { $fmanager->deleteFile($sitemapfile); }

	// Remove toolbar icons
    $toolbar = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_articles.png';
    if (file_exists($toolbar)) { $fmanager->deleteFile($toolbar); }
    $toolbar = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_articles_f2.png';
    if (file_exists($toolbar)) { $fmanager->deleteFile($toolbar); }
    $toolbar = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_magazines.png';
    if (file_exists($toolbar)) { $fmanager->deleteFile($toolbar); }
    $toolbar = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_magazines_f2.png';
    if (file_exists($toolbar)) { $fmanager->deleteFile($toolbar); }
    $toolbar = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_config.png';
    if (file_exists($toolbar)) { $fmanager->deleteFile($toolbar); }
    $toolbar = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_config_f2.png';
    if (file_exists($toolbar)) { $fmanager->deleteFile($toolbar); }
    
	// Remove admin language
    $adlang = $mainframe->getCfg('absolute_path').'/administrator/language/english/english.com_arthrology.php';
    if (file_exists($adlang)) { $fmanager->deleteFile($adlang); }
    $adlang = $mainframe->getCfg('absolute_path').'/administrator/language/greek/greek.com_arthrology.php';
    if (file_exists($adlang)) { $fmanager->deleteFile($adlang); }
    $adlang = $mainframe->getCfg('absolute_path').'/administrator/language/italian/italian.com_arthrology.php';
    if (file_exists($adlang)) { $fmanager->deleteFile($adlang); }
}

?>
