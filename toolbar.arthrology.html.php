<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * Backend Toolbar HTML Event Handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html				
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/***********************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S TOOLBAR FUNCTIONALITY  */
/***********************************************************************/
class clsArthrologyToolbarHTML {
	// Initialize variables

	// Create cp menu
	static public function _CONF() {
		global $adminLanguage;
		
		mosMenuBar::startTable();
		mosMenuBar::custom( 'conf_save', 'save.png', 'save_f2.png', $adminLanguage->A_SAVE, false );
		mosMenuBar::spacer();
		mosMenuBar::custom( 'conf_apply', 'apply.png', 'apply_f2.png', $adminLanguage->A_APPLY, false );
		mosMenuBar::spacer();
		mosMenuBar::cancel( 'conf_cancel' );
		mosMenuBar::endTable();
	}

	static public function _CP() {
		global $adminLanguage;
		
		mosMenuBar::startTable();
		mosMenuBar::custom( 'list', 'arthrology_articles.png', 'arthrology_articles_f2.png', $adminLanguage->A_CMP_ARTHROLOGY_TOOLBAR_ARTICLES, false );
		mosMenuBar::spacer();
		mosMenuBar::custom( 'cat', 'arthrology_magazines.png', 'arthrology_magazines_f2.png', $adminLanguage->A_CMP_ARTHROLOGY_TOOLBAR_MAGAZINES, false );
		mosMenuBar::spacer();
		mosMenuBar::divider();
		mosMenuBar::spacer();
		mosMenuBar::custom( 'conf', 'arthrology_config.png', 'arthrology_config_f2.png', $adminLanguage->A_CMP_ARTHROLOGY_TOOLBAR_CONFIG, false );
		mosMenuBar::endTable();
	}

	// Create event list menu
	static public function _LIST() {
		mosMenuBar::startTable();
		mosMenuBar::publishList();
		mosMenuBar::spacer();
		mosMenuBar::unpublishList();
		mosMenuBar::spacer();
		mosMenuBar::addNew();
		mosMenuBar::spacer();
		mosMenuBar::editList();
		mosMenuBar::spacer();
		mosMenuBar::spacer();
		mosMenuBar::deleteList();
		mosMenuBar::endTable();
	}
	
	// Create edit/add event form
	static public function _EDIT(){
		mosMenuBar::startTable();
		mosMenuBar::save();
		mosMenuBar::spacer();
		mosMenuBar::apply();
		mosMenuBar::spacer();
		mosMenuBar::cancel();
		mosMenuBar::endTable();
	}
}
?>
