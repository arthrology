<?php

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * SEO handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html			
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Declare globals
global $mainframe;

// Include seovs class
require_once($mainframe->getCfg('absolute_path').'/administrator/includes/seovs.class.php');

/*********************************************************/
/*  THE CLASS THAT WILL CONTAIN THE SEOVS FUNCTIONALITY  */
/*********************************************************/
class seovsArthrology extends seovs {

    public $option = '';
    public $title = '';
    public $seotitle = '';
    public $ascii = '';
    public $id = 0;
    public $catid = 0;
    public $Itemid = 0;
    public $message = '';
    public $code = 0;
    public $section = '';
    
	/********************************/
	/*  Implement suggest function  */
	/********************************/
	public function suggest() {
    	global $mosConfig_absolute_path;

        if ($this->title == '') {
            $this->writeMsg('1');
            return false;
        }
        $ascii = strtolower(eUTF::utf8_to_ascii($this->title, ''));
        $this->ascii = preg_replace("/[^a-zA-Z0-9-_\s]/", '', $ascii);
        if ($this->ascii == '') {
            $this->writeMsg('1');
            return false;
        }

        $parts = split(' ', $this->ascii);
        $nparts = array();
        foreach ($parts as $part) {
            if (strlen($part) > 1) { array_push($nparts, $part); }
        }
        if (count($nparts) == 0) {
            $this->writeMsg('1');
            return false;
        }
        $newtitle = implode('-', $nparts);

		$invalidNames = array('save', 'cancel');
		if (in_array($newtitle, $invalidNames) || preg_match('/^(com_)/', $newtitle)) {
			$newtitle = 'arthrology-'.$newtitle;
		} else if (file_exists($mosConfig_absolute_path.'/'.$newtitle.'/')) {
			$newtitle = 'arthrology-'.$newtitle;
		}
		
        return $this->suggest_articles($newtitle);
    }

	/***************************************************/
	/*  New suggest_events function for EventCalendar  */
	/***************************************************/
    protected function suggest_articles($ntitle) { //feed manually id and catid before suggest
        global $database;

        if (intval($this->catid) < 1) {
            $this->writeMsg('6');
            return false;
        }

        $extra = ($this->id) ? " AND id <> '".$this->id."'" : '';
        $database->setQuery("SELECT COUNT(*) FROM #__arthrology WHERE catid='".$this->catid."' AND seotitle='".$ntitle."'".$extra);
        $c = intval($database->loadResult());
        if ($c) {
            for ($i=2; $i<21; $i++) {
                $database->setQuery("SELECT COUNT(*) FROM #__arthrology WHERE catid='".$this->catid."' AND seotitle='".$ntitle."-".$i."'".$extra);
                $c2 = intval($database->loadResult());
                if (!$c2) {
                    $ntitle .= '-'.$i;
                    break;
                }
            }
            if ($c2) { $ntitle .= '-'.rand(1000,9999); }
        }

        $this->sugtitle = $ntitle;
        return $ntitle;
    }

	/*********************************/
	/*  Implement validate function  */
	/*********************************/
    public function validate() {
        global $mosConfig_absolute_path;

		$invalidNames = array('save', 'cancel');
		if ( in_array($this->seotitle, $invalidNames) || preg_match('/^(com_)/', $this->seotitle) || file_exists($mosConfig_absolute_path.'/'.$this->seotitle.'/') ) {
            $this->writeMsg('2');
            return false;
		}
		
        if ($this->seotitle == '') {
            $this->writeMsg('5');
            return false;
        }
        if (!eUTF::utf8_isASCII($this->seotitle)) {
            $this->writeMsg('2');
            return false;
        }
        $seotitle2 = preg_replace("/[^a-z0-9-_]/", '', $this->seotitle);
        if ($seotitle2 != $this->seotitle) { 
            $this->writeMsg('2');
            return false;
        }

        return $this->validate_articles();
    }

	/*********************************************/
	/*  New validate function for EventCalendar  */
	/*********************************************/
    protected function validate_articles() { //feed manually id, catid before validation
        global $database;

        if (intval($this->catid) < 1) {
            $this->writeMsg('6');
            return false;
        }

        $extra = ($this->id) ? " AND id <> '".$this->id."'" : '';
        $database->setQuery("SELECT COUNT(*) FROM #__arthrology WHERE catid='".$this->catid."' AND seotitle='".$this->seotitle."'".$extra);
        $c = intval($database->loadResult());
	    if ($c) {
		    $this->writeMsg('3');
            return false;
        }
        $this->writeMsg('4');
        return true;
    }
}
?>
