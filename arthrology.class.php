<?php

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x+
 *
 * Event handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html			
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

if (!defined('ARTHBASE')) {
	global $_VERSION;
	if (($_VERSION->RELEASE >= 2009) && ($_VERSION->DEV_LEVEL >= 1)) {
		define('ARTHBASE', 'arthrology');
	} else {
		define('ARTHBASE', 'com_arthology');
	}
}

/*********************************************************/
/*  THE CLASS THAT WILL CONTAIN THE EVENT FUNCTIONALITY  */
/*********************************************************/
class mosArthrology_Article extends mosDBTable {
	
	// Declare variables
	public $id;
	public $author;
	public $title;
	public $seotitle;
	public $catid;
	public $year;
	public $pages;
	public $description;
	public $tags;
	public $published;
	public $checked_out = '0';
	public $checked_out_time = '1979-12-19 00:00:00';
	public $params;
	
	public $category;
	public $cat_params;
	
	/*****************/
	/*  Constructor  */
	/*****************/
    public function __construct($database) {
        $this->mosDBTable( '#__arthrology', 'id', $database);
    }

	/******************************************************/
	/*  Implement load function to have additional stuff  */
	/******************************************************/
	function load( $oid = null ) {
	
		$k = $this->_tbl_key;
		if ($oid !== null) {
			$this->$k = $oid;
		}

		$oid = $this->$k;
		if ($oid === null) {
			return false;
		}

		$class_vars = get_class_vars( get_class( $this ) );
		foreach ($class_vars as $name => $value) {
			if (($name != $k) and ($name != "_db") and ($name != "_tbl") and ($name != "_tbl_key")) {
				$this->$name = $value;
			}
		}

		$query = "SELECT e.*, c.name AS category, c.params AS cat_params"
			. "\n FROM $this->_tbl e"
			. "\n LEFT JOIN #__categories c ON c.id = e.catid"
			. "\n WHERE e.$this->_tbl_key = $oid";
		$this->_db->setQuery( $query );

		return $this->_db->loadObject( $this );
	}

	/*******************************************/
	/*  Implement the database check function  */
	/*******************************************/
	function check( $error_msg ) {
		$error_msg = "";

		return true;
	}

	/*******************************************/
	/*  Implement the database store function  */
	/*******************************************/
	function store( $updateNulls=false ) {
		$k = $this->_tbl_key;

		//eliminate the unvalid params
		$this->category	= null;
		$this->cat_params = null;

		if ($this->$k) {
			$ret = $this->_db->updateObject( $this->_tbl, $this, $this->_tbl_key, $updateNulls );
		} else {
			$ret = $this->_db->insertObject( $this->_tbl, $this, $this->_tbl_key );
		}
		if( !$ret ) {
			$this->_error = strtolower( get_class( $this ) ) . '::store failed <br />' . $this->_db->getErrorMsg();
			return false;
		} else {
			return true;
		}
	}

	/******************************************/
	/*  Implement the database bind function  */
	/******************************************/
	//de- / encoding of some passed variables as dates to timestamps and arrays to strings is necessary
	function bind( $array, $ignore='' ) {
		global $database;

		if (!is_array( $array )) {
			$this->_error = strtolower(get_class( $this ))."::bind failed.";
			return false;
		}

		//check if articles for this category have to be published by an administrator
		$category = new mosCategory ( $database );
		$category->load( $array['catid'] );
		$parameter = new mosParameters( $category->params );
		if ($parameter->get( 'autopublish' )) {
			$array['published'] = '1';
		}

		return mosBindArrayToObject( $array, $this, $ignore );
	}

	/***********************************************************/
	/*  Bind this object to an array of a raw database result  */
	/***********************************************************/
	function bindRaw( $array, $ignore='' ) {
		return mosBindArrayToObject( $array, $this, $ignore, null, false );
	}
}
