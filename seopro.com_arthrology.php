<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x+
 *
 * SEO Pro Handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html			
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

if (!defined('ARTHBASE')) {
	global $_VERSION;
	if (($_VERSION->RELEASE >= 2009) && ($_VERSION->DEV_LEVEL >= 1)) {
		define('ARTHBASE', 'arthrology');
	} else {
		define('ARTHBASE', 'com_arthrology');
	}
}

// Generate SEO Pro page
function seogen_com_arthrology($link) {
	global $mosConfig_live_site, $database;

	if (trim($link) == '') { return ''; }

	$database->setQuery( "SELECT params FROM #__components WHERE link = 'option=com_arthrology'" );
	$text = $database->loadResult();
	$config_values = new mosParameters( $text );

	$vars = array();
	$vars['option'] = 'com_arthrology';
	$vars['task'] = '';
	$vars['Itemid'] = '';
	$half = preg_split('/[\?]/', $link);
	if (isset($half[1])) {
		$half2 = preg_split('/[\#]/', $half[1]);
		$parts = preg_split('/[\&]/', $half2[0], -1, PREG_SPLIT_NO_EMPTY);
		if (count($parts) >0) {
			foreach ($parts as $part) {
				list($x, $y) = preg_split('/[\=]/', $part, 2);
				$x = strtolower($x);
				$vars[$x] = $y;
			}
		}
	}
	
	// Generate SEO Pro page depending task
	switch ($vars['task']) {
		case 'search':
			return $mosConfig_live_site.'/'.ARTHBASE.'/search.html';
		break;
		case 'results':
			return $mosConfig_live_site.'/'.ARTHBASE.'/results.html';
		break;
		default:
			return $mosConfig_live_site.'/'.ARTHBASE.'/';
		break;
	}
}

// Restore original page
function seores_com_arthrology($seolink='', $register_globals=0) { 
	global $database, $lang;

	$seolink = urldecode($seolink);
	$seolink = trim(preg_replace('/(&amp;)/', '&', $seolink));
	$link = preg_split('/[\?]/', $seolink);
	$itemsyn = intval(mosGetParam( $_SESSION, 'itemsyn', 0 ));
	
	$QUERY_STRING = array();
	
	$_GET['option'] = 'com_arthrology';
	$_REQUEST['option'] = 'com_arthrology';
	$QUERY_STRING[] = 'option=com_arthrology';

	if (($link[0] == ARTHBASE.'/') OR ($link[0] == ARTHBASE)) {					// URL = ../arthrology/
		$task = 'search';
		$_GET['task'] = $task;
		$_REQUEST['task'] = $task;
		$QUERY_STRING[] = 'task='.$task;
	} else {																	// ULR = ../arthrology/results.html
		$parts = preg_split('/[\/]/', $link[0]);								//    or ../arthrology/search.html
		$part = preg_replace('/(\.html)$/', '', $parts[1]);
		$task = $part;
		$_GET['task'] = $part;
		$_REQUEST['task'] = $part;
		$QUERY_STRING[] = 'task='.$part;
	}

	$query = "SELECT id FROM #__menu WHERE link='index.php?option=com_arthrology' AND published='1'"
			."\n AND ((language IS NULL) OR (language LIKE '%$lang%'))";

	if ($itemsyn) { $_Itemid = $itemsyn; }
	if (isset($_Itemid) && ($_Itemid > 0)) {
		$Itemid = $_Itemid;
	} else {
		$database->setQuery($query, '#__', 1, 0);
		$Itemid = intval($database->loadResult());
	}

	$_GET['Itemid'] = $Itemid;
	$_REQUEST['Itemid'] = $Itemid;
	$QUERY_STRING[] = 'Itemid='.$Itemid;

    $qs = '';
    if (count($QUERY_STRING) > 0) { $qs = implode('&',$QUERY_STRING); }
	if (trim($link[1]) != '') { $qs .= ($qs == '') ? $link[1] : '&'.$link[1]; }

    $_SERVER['QUERY_STRING'] = $qs;
	$_SERVER['REQUEST_URI'] = ($qs != '') ? '/index.php?'.$qs : '/index.php';
}

?>
