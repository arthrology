<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x+
 *
 * Backend Event Handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Check if the user is allowed to access this component. If not then re-direct him to the administration's front-page.
if (!($acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'all' )
	| $acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'com_arthrology' ))) {
	mosRedirect( 'index2.php', $adminLanguage->A_NOT_AUTH );
}

// Includes
require_once($mainframe->getCfg('absolute_path' ).'/components/com_arthrology/arthrology.class.php'); // Component's general classes
require_once($mainframe->getCfg('absolute_path' ).'/administrator/components/com_arthrology/admin.arthrology.html.php'); // Component's html file for the administration area
require_once($mainframe->getCfg('absolute_path' ).'/administrator/includes/pageNavigation.php'); // ElxisCore::pageNavigation component

/*************************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S BACK-END FUNCTIONALITY  */
/*************************************************************************/
class clsArthrologyAdmin {
	
	// Initialize variables
	public $task = ''; // component's current task
	public $apath = ''; // component's back-end absolute path
	public $hpath = ''; // component's front-end absolute path
	public $live_apath = ''; // component's back-end absolute path
	public $live_hpath = ''; // component's front-end absolute path
	public $art_id = '';
	public $catid = '';
	public $cid = '';
	public $field = '';
	public $order = '';
	
	/****************************/
	/*  The class' constructor  */
	/****************************/ 
	public function __construct() {
		global $alang, $mainframe; $mosConfig_live_site; // Pass global variables or objects

        // Set variables
        $this->task = htmlspecialchars(mosGetParam($_REQUEST, 'task', 'cp'));
        $this->apath = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology';
        $this->hpath = $mainframe->getCfg('absolute_path').'/components/com_arthrology';
        $this->live_apath = $mainframe->getCfg('live_site').'/administrator/components/com_arthrology';
        $this->live_hpath = $mainframe->getCfg('live_site').'/components/com_arthrology';
		$this->art_id = mosGetParam( $_REQUEST, 'cid', array(0) );
		$this->catid = mosGetParam( $_REQUEST, 'catid', '0' );
		$this->cid = $art_id;
		$this->field = mosGetParam($_REQUEST, 'field', false);
		$this->order = mosGetParam($_REQUEST, 'order', 'none');

	}

	/******************************/
	/*  The class' main function  */
	/******************************/ 	
	public function main() {
	
		switch ($this->task) {
			// Configuration
			case 'conf':
				$this->showConfiguration();
				break;
			case 'conf_cancel':
				$this->showControlPanel();
				break;
			case 'conf_apply':
			case 'conf_save':
				$this->saveConfiguration();
				break;
			case 'conf_import':
				$this->importCVS();
				break;
			
			// Articles
			case 'list':
				$this->listArticles();
				break;
			case 'edit':
				$this->art_id = (is_array($this->art_id))?$this->art_id[0]:$this->art_id;
				$this->editArticles($this->art_id);
				break;
			case 'new':
				$this->editArticles();
				break;
			case 'save':
			case 'apply':
				$this->saveArticles();
				break;
			case 'cancel':
				$this->cancelArticles();
				break;
			case 'publish':
				$this->publishArticles($this->art_id, 1);
				break;
			case 'unpublish':
				$this->publishArticles($this->art_id, 0);
				break;
			case 'remove':
				$this->deleteArticles($this->art_id);
				break;
			
			case 'cat':
				mosRedirect( 'index2.php?option=com_categories&section=com_arthrology' );
				break;
			
			// AJAX
			case 'validate':
				$this->validateSEO();
				break;
			case 'suggest':
			    $this->suggestSEO();
				break;
			case 'ajaxpub':
				$this->ajaxchangeContent();
				break;
			
			// Default action
			case 'cp':
			default:
				$this->showControlPanel();
				break;
		}
	}

	/***********************************************************************************/
	/*  Prepare to show Configuration Panel								CONFIGURATION  */
	/***********************************************************************************/
	protected function showConfiguration() {
		global $database;
		
		// Load params values
		$database->setQuery( "SELECT params FROM #__components WHERE link = 'option=com_arthrology'" );
		$text = $database->loadResult();
		$config_values = new mosParameters( $text );
		
		clsArthrologyAdminHTML::showConfigurationHTML( $config_values );
	}
	
	/***********************************************************************************/
	/*  Save configuration												CONFIGURATION  */
	/***********************************************************************************/
	protected function saveConfiguration() {
		global $database, $adminLanguage; // Pass global variables or objects

		// Initialize variables
		$params = new mosParameters('');
		
		// Get group_role_permissions from Post-Data
		$group_roles_new_auto = mosGetParam( $_REQUEST, 'newauto', array('') );
		$group_roles_new_all = mosGetParam( $_REQUEST, 'newall', array('') );
		$group_roles_changes_auto = mosGetParam( $_REQUEST, 'changesauto', array('') );
		$group_roles_changes_all = mosGetParam( $_REQUEST, 'changesall', array('') );
			$params->set( 'newauto', implode( ',', $group_roles_new_auto ) );
			$params->set( 'newall', implode( ',', $group_roles_new_all ) );
			$params->set( 'changesauto', implode( ',', $group_roles_changes_auto ) );
			$params->set( 'changesall',	implode( ',', $group_roles_changes_all ) );

		// Get the rest settings
		$limit = intval($this->makesafe(mosGetParam( $_REQUEST, 'limit', 25 )));
			$params->set( 'limit', $limit );

		// Store params to database
		$params_as_text = array();
		foreach ($params->_params as $k=>$v) {
			$params_as_text[] = "$k=$v";
		}
		$database->setQuery( "UPDATE #__components SET params = \n'" . mosParameters::textareaHandling( $params_as_text ) . "'\n WHERE link = 'option=com_arthrology'" );
		if ($database->query()) {
			$msg = $adminLanguage->A_CMP_ARTHROLOGY_CONF_MSG_SUCCESS;
		} else {
			$msg = $adminLanguage->A_CMP_ARTHROLOGY_CONF_MSG_FAIL;
		}

		// Check if task is apply or save and redirect respectively
		switch ( $this->task ) {
			case 'conf_apply':
				mosRedirect( 'index2.php?option=com_arthrology&task=conf', $msg );
				break;
			case 'conf_save':
			default:
				mosRedirect( 'index2.php', $msg );
				break;
		}
	}

	/***********************************************************************************/
	/*  Import from CVS													CONFIGURATION  */
	/***********************************************************************************/
	protected function importCVS() {
		global $database; // Pass global variables or objects

		// Initialize variables
		$cvs = mosGetParam( $_REQUEST, 'cvs');
			$cvs = $this->makesafe($cvs);
			if ($cvs == '') {
				echo "<script type=\"text/javascript\">alert('".$adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE.": ".$adminLanguage->A_CMP_ARTHROLOGY_CP_ALERT_NO_CVS."'); window.history.go(-1);</script>"._LEND;
			}
		$catid = mosGetParam( $_REQUEST, 'catid' );
			$catid = $this->makesafe($catid);
			if ( ($catid == '') || ($catid == '0') ) {
				echo "<script type=\"text/javascript\">alert('".$adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE.": ".$adminLanguage->A_CMP_ARTHROLOGY_CP_ALERT_NO_MAGAZINE."'); window.history.go(-1);</script>"._LEND;
			}
		
		$published = $this->makesafe(mosGetParam( $_REQUEST, 'published', 1 ));
		
		$cvs = split(_LEND, $cvs);
		foreach($cvs as $line) {
			$art = split(',,', $line);
			$new['author'] = $art[0];
			$new['title'] = $art[1];
			$new['pages'] = $art[2];
			$new['year'] = $art[3];
			$new['catid'] = $catid;
				$coid = 0;
				$cocatid = $catid;
				$cotitle = $art[1];

				require_once($this->apath.'/arthrology.seovs.class.php');
				$seo = new seovsArthrology('com_arthrology', $cotitle);
				$seo->id = $coid;
				$seo->catid = $cocatid;
			$new['seotitle'] = $seo->suggest();
			$new['published'] = $published;
		
			$article = new mosArthrology_Article($database);
			$article->bind( $new );
			if ($article->check($error)) {
				if (!$article->store()) {
					echo "<script type=\"text/javascript\">alert('".$adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE.": ".$adminLanguage->A_CMP_ARTHROLOGY_CP_ALERT_FAIL."'); window.history.go(-1);</script>"._LEND;
					exit();
				}
			} else {
				echo "<script type=\"text/javascript\">alert('".$adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE.": ".$error."'); window.history.go(-1);</script>"._LEND;
				exit();
			}
		} //end foreach
		
		$msg = count($cvs) . $adminLanguage->A_CMP_ARTHROLOGY_CP_ALERT_SUCCESS;
		
		// Redirect to Control Panel
		mosRedirect( 'index2.php?option=com_arthrology', $msg );
	}

	/***********************************************************************************/
	/*  Prepare to show Control Panel  									CONTROL PANEL  */
	/***********************************************************************************/
	protected function showControlPanel() {
		
		// Show ControlPanel html
		clsArthrologyAdminHTML::showControlPanelHTML();
	}
	
	/**********************************/
	/*  Prepare to show article list  */
	/**********************************/
	protected function listArticles () {
		global $mainframe, $database, $mosConfig_list_limit, $adminLanguage; // Pass global variables or objects

		if ( $this->field && $this->order <> "none" ) {
			$ordering = "\n ORDER BY e.$this->field " . $this->order;
		} else {
			$ordering = "\n ORDER BY e.title, c.title, e.year";
		}

		if (isset($searchstring)) $searcharray[] = $searchstring;
		
		if(($this->catid <> 0) && is_numeric($this->catid) ) {
			$searcharray[] = " e.catid=" . $this->catid;
		}

		if ($this->task == "unpublished") {$searcharray[] = "e.published = '0'";}

		//filter-options
		$search = mosGetParam($_REQUEST, "search", false);
		if ($search) $searcharray[] = "(e.title LIKE '%$search%')";
		$searchstring = (@$searcharray)?implode(" AND ", $searcharray):"1";
		$database->setQuery( "SELECT COUNT(*) FROM #__arthrology e WHERE $searchstring");

		// page navigation
		$total = $database->loadResult();
		$limitstart = $mainframe->getUserStateFromRequest( "view{arthrology}", 'limitstart', 0 );
		$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', $mosConfig_list_limit );    
		$pageNav = new mosPageNav( $total, $limitstart, $limit );

		$database->setQuery(
			"SELECT e.*, c.title AS cat_name, c.params AS cat_params FROM #__arthrology e" .
			"\n LEFT JOIN #__categories c ON c.id = e.catid" .
			"\n WHERE " . $searchstring .
			$ordering, '#__', $pageNav->limit, $pageNav->limitstart
		);
		$results = $database->loadObjectList();
		$categories[] = mosHTML::makeOption( "0", $adminLanguage->A_CMP_ARTHROLOGY_LIST_ALL_ARTICLES );
		$categories[] = mosHTML::makeOption( "old", $adminLanguage->A_CMP_ARTHROLOGY_LIST_OLD_ARTICLES );
		
		$query = "SELECT id AS value, name AS text"
			. "\n FROM #__categories"
			. "\n WHERE section = 'com_arthrology'"
			. "\n AND published = '1'";
		$database->setQuery( $query );
		$categories = array_merge( $categories, $database->loadObjectList() );

		$catlist = mosHTML::selectList( $categories, 'catid', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $this->catid );

		clsArthrologyAdminHTML::listArticlesHTML($results, $pageNav, $catlist );	
	}

	/*******************************/
	/*  Prepare to edit/add event  */
	/*******************************/
	protected function editArticles($existing = NULL) {
		global $my, $database;

		if (is_numeric($existing)) {
			$article = new mosArthrology_Article($database);
			$article->load($existing);
			$existing = $article;
			$existing->checkout( $my->id );
		}
		clsArthrologyAdminHTML::editArticleHTML($existing);	
	}
	
	/****************/
	/*  Save event  */
	/****************/
	protected function saveArticles() {
		global $database, $adminLanguage, $my;

		//CSRF prevention
		$tokname = 'token'.$my->id;
		if ( !isset($_POST[$tokname]) || !isset($_SESSION[$tokname]) || ($_POST[$tokname] != $_SESSION[$tokname]) ) {
			die( 'Detected CSRF attack! Someone is forging your requests.' );
		}
				
		$article = new mosArthrology_Article($database);
		$article->bind( $_POST );
		if ($article->check($error)) {
			if ($article->store()) {
				$msg = $adminLanguage->A_CMP_ARTHROLOGY_EDIT_MSG_SUCCESS;
			} else {
				echo "<script type=\"text/javascript\">alert('".$adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE.": ".$adminLanguage->A_CMP_ARTHROLOGY_EDIT_MSG_FAIL."'); window.history.go(-1);</script>"._LEND;
				exit();
			}
		} else {
			echo "<script type=\"text/javascript\">alert('".$adminLanguage->A_CMP_ARTHROLOGY_GEN_COMPONENT_TITLE.": ".$error."'); window.history.go(-1);</script>"._LEND;
			exit();
		}

		// Validate SEO
		require_once($this->apath.'/arthrology.seovs.class.php');
		$seo = new seovsArthrology('com_arthrology', '', $article->seotitle);
		$seo->id = intval($article->id);
		$seo->catid = intval($article->catid);
		$seoval = $seo->validate();
		if (!$seoval) {
			echo "<script type=\"text/javascript\">alert('".$adminLanguage->A_SEOTITLE.": ".$seo->message."'); window.history.go(-1);</script>"._LEND;
			exit();
		}
    
		// Check if task is apply or save and redirect respectively
		switch ( $this->task ) {
			case 'apply':
				mosRedirect( 'index2.php?option=com_arthrology&task=edit&hidemainmenu=1&cid='.mosGetParam( $_POST, 'id'), $msg );
				break;
			case 'save':
			default:
				mosRedirect( 'index2.php?option=com_arthrology', $msg );
				break;
		}	
	}
	
	/*********************/
	/*  Cancel add/edit  */
	/*********************/
	protected function cancelArticles() {
		global $database;
		
		$article = new mosArthrology_Article($database);
		$article->checkin($_POST['id']); //the id is defined globally
		$this->catid = 0;
		
		$this->listArticles();
	}
	
	/******************/
	/*  Delete event  */
	/******************/
	protected function deleteArticles($itemids) {
		global $database;
		
		$article = new mosArthrology_Article($database);
		foreach($itemids AS $itemid) {
			$article->delete($itemid);	
		}
		
		$this->listArticles();
	}
	
	/***********************/
	/*  Un-/Publish event  */
	/***********************/
	protected function publishArticles($art_id, $value) {
		global $database;
		
		$event = new mosArthrology_Article($database);
		foreach($art_id AS $id) {
		  $event->load($id);
		  $event->published = ''.intval($value).'';
		  $event->store();
		}
		$this->listArticles();
	}

	/*****************************/
	/* Validate SEO Title (AJAX) */
	/*****************************/
	protected function validateSEO() {
		global $mainframe;

		$coid = intval(mosGetParam($_POST, 'coid', 0));
		$cocatid = intval(mosGetParam($_POST, 'cocatid', 0));
		$seotitle = eUTF::utf8_trim(mosGetParam($_POST, 'seotitle', ''));

		require_once($this->apath.'/arthrology.seovs.class.php');
		$seo = new seovsArthrology('com_arthrology', '', $seotitle);
		$seo->id = $coid;
		$seo->catid = $cocatid;
		$seo->validate();
		echo $seo->message;
		exit();
	}

	/****************************/
	/* Suggest SEO Title (AJAX) */
	/****************************/
	protected function suggestSEO() {
		global $mainframe;

		$coid = intval(mosGetParam($_POST, 'coid', 0));
		$cocatid = intval(mosGetParam($_POST, 'cocatid', 0));
		$cotitle = mosGetParam($_POST, 'cotitle', '');

		require_once($this->apath.'/arthrology.seovs.class.php');
		$seo = new seovsArthrology('com_arthrology', $cotitle);
		$seo->id = $coid;
		$seo->catid = $cocatid;
		$sname = $seo->suggest();

		@ob_end_clean();
		@header('Content-Type: text/plain; Charset: utf-8');
		if ($sname) {
			echo '|1|'.$sname;
		} else {
			echo '|0|'.$seo->message;
		}
		exit();
	}

	/*******************************/
	/* Change publish state (AJAX) */
	/*******************************/
	function ajaxchangeContent() {
		global $database, $my, $adminLanguage;

		$elem = intval(mosGetParam($_REQUEST, 'elem', 0));
		$id = intval(mosGetParam($_REQUEST, 'id', 0));
		$state = intval(mosGetParam($_REQUEST, 'state', 0));

		if (!$id) {
			echo '<img src="../includes/js/ThemeOffice/warning.png" width="16" height="16" border="0" title="'.$adminLanguage->A_ERROR.': Invalid Item id" />'._LEND;
			exit();
		}

		$error = 0;
		$database->setQuery( "UPDATE #__arthrology SET published='$state' WHERE id='$id' AND (checked_out=0 OR (checked_out='".$my->id."'))");
		if (!$database->query()) { $error = 1; }

		if ($error) { $state = $state ? 0 : 1; }
		$img = $state ? 'publish_g.png' : 'publish_x.png';
		$alt = $state ? $adminLanguage->A_PUBLISHED : $adminLanguage->A_UNPUBLISHED;	?>
		<a href="javascript: void(0);" 
		onclick="changeContentState('<?php echo $elem; ?>', '<?php echo $id; ?>', '<?php echo ($state) ? 0 : 1; ?>');" title="<?php echo $alt; ?>">
		<img src="images/<?php echo $img; ?>" width="12" height="12" border="0" alt="<?php echo $alt; ?>" /></a>
		<?php exit();
	}
	
	/***********************/
	/*  Make strings safe  */
	/***********************/
	public function makesafe($string='', $strict=1) {
		// Special thanks to Ioannis <datahell> Sannos for this
		
	    if ($string == '') { return $string; }
        if ($strict) {
            $pat = "([\']|[\!]|[\(]|[\)]|[\;]|[\"]|[\$]|[\#]|[\<]|[\>]|[\*]|[\%]|[\~]|[\`]|[\^]|[\|]|[\{]|[\}]|[\\\])";
        } else {
            $pat = "([\']|[\"]|[\$]|[\#]|[\<]|[\>]|[\*]|[\%]|[\~]|[\`]|[\^]|[\|]|[\{]|[\}]|[\\\])";
        }
        $s = eUTF::utf8_trim(preg_replace($pat, '', $string));
        return $s;
	}
}

// Initiate the class/ and execute it, then unset the 
// object in order to free the allocated PHP memory.
$objArthrology = new clsArthrologyAdmin();
$objArthrology->main();
unset($objArthrology);

?>
