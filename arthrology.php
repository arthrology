<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x+
 *
 * Frontend Event Handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

//  Check the permissions
if ( $my->usertype == '' ) { 
	$usertype = eUTF::utf8_strtolower($acl->get_group_name('29'));
} else {
	$usertype = eUTF::utf8_strtolower($my->usertype);
}
// Check if the user is allowed to access this component. If not then re-direct him to the main page.
if (($mosConfig_access == '1') | ($mosConfig_access == '3')) {
	if (!($acl->acl_check( 'action', 'view', 'users', $usertype, 'components', 'all' ) || 
	    $acl->acl_check( 'action', 'view', 'users', $usertype, 'components', 'com_arthrology' ))) {
		    mosRedirect( 'index.php', _NOT_AUTH );
	}
}

// Includes
require_once($mainframe->getCfg('absolute_path' ).'/components/com_arthrology/arthrology.class.php'); // Component's general class
require_once($mainframe->getCfg('absolute_path' ).'/components/com_arthrology/arthrology.html.php'); // Component's html file for the public area

/*************************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S FRONT-END FUNCTIONALITY  */
/*************************************************************************/
class clsArthrology {
	
	// Initialize variables
	public $live_path = ''; // component's live path
	public $abs_path = ''; // component's absolute path
	public $lng = null; // component's language
	public $params = array(); // component's parameters

	public $task = '';
	public $keyword = '';
	public $catid = '';

	/****************************/
	/*  The class' constructor  */
	/****************************/ 
	public function __construct() {
		global $mainframe, $lang, $Itemid, $database;

		// Set absolute and live path
		$this->live_path = $mainframe->getCfg('live_site').'/components/com_arthrology';
		$this->abs_path = $mainframe->getCfg('absolute_path').'/components/com_arthrology';

		// Load language file
		if (file_exists($this->abs_path.'/language/'.$lang.'.php')) {
			require_once($this->abs_path.'/language/'.$lang.'.php');
		} else {
			require_once($this->abs_path.'/language/english.php');
		}
		$this->lng = new clsArthrologyLng();
		
		// Load params from Arthrology (component)
			$database->setQuery("SELECT params FROM #__components WHERE link = 'option=com_arthrology'", '#__', 1, 0);
			$result = $database->loadResult();
			$com_params = new mosParameters($result);
		// Load params from MENUITEM
			$menu = new mosMenu($database);
			$menu->load($Itemid);
			$menu_params = new mosParameters($menu->params);
		// Create an array with parameters
		$this->params['com'] = $com_params;
		$this->params['menu'] = $menu_params;
		$this->params['menuobj'] = $menu;

		//Set current task and keyword
		$this->task = $this->makesafe((string)mosGetParam($_REQUEST, 'task' , 'search'));


		$this->keyword = $this->makesafe((string)mosGetParam($_POST, 'keyword' , NULL));
			if ($this->keyword == '') {
				$this->keyword = $this->makesafe((string)mosGetParam($_REQUEST, 'keyword' , NULL));
			}
		$this->catid = $this->makesafe((string)mosGetParam($_POST, 'catid', NULL));
			if ($this->catid == '') {
				$this->catid = $this->makesafe((string)mosGetParam($_REQUEST, 'catid' , NULL));
			}
	}

	/******************************/
	/*  The class' main function  */
	/******************************/ 	
	public function main() {
		global $Itemid;

		clsArthrologyHTML::prepareHTML();
		
		switch($this->task) {
			case 'results':
			case 'printlist':
				$this->showResults();
				break;
			case 'search':
				clsArthrologyHTML::searchScreenHTML();
				break;
			default: // Redirect to search mode
				mosRedirect( sefRelToAbs("index.php?option=com_arthrology&task=search&Itemid=".$Itemid, ARTHBASE."/search.html") );
				break;
		}
	}

	/*****************************/
	/*  Prepare to show results  */
	/*****************************/
	private function showResults() {
		global $database, $mainframe;

		if (eUTF::utf8_strlen($this->keyword) >= 3) {
			$like_clause = "\n AND ((e.title LIKE '%".$this->keyword."%') OR (e.tags LIKE '%".$this->keyword."%') OR (e.description LIKE '%".$this->keyword."%') OR (e.author LIKE '%".$this->keyword."%'))";
		} else {
			$like_clause = "";
		}
	
		if ($this->catid) {
			$where_clause = "\n WHERE e.published = '1' AND c.id = '" . $this->catid . "'";
		} else {
			$where_clause = "\n WHERE e.published = '1'";
		}

		// Load page navigation
		$query = "SELECT COUNT(e.id) FROM #__arthrology e"
			."\n INNER JOIN #__categories c ON c.id = e.catid"
			.$where_clause
			.$like_clause;
		$database->setQuery($query);
		$total = intval($database->loadResult());

		if ($this->task != 'printlist') {
			$limit = $this->params['com']->get('limit', 25);

			if ($mainframe->getCfg('sef') == 2) {
				$page = intval(mosGetParam($_REQUEST, 'page', 0));
				if ($page < 1) { $page = 0; }
				$limitstart = ($page * $limit);
			}
			if ( $total <= $limit ) { $limitstart = 0; }
		} else {
			$limit = $total;
			$limitstart = 0;
		}

		require_once($mainframe->getCfg('absolute_path').'/includes/pageNavigation.php');
		$pageNav = new mosPageNav($total, $limitstart, $limit);	

		// Load rows
		$query = "SELECT e.*, c.title AS cat_name, c.description AS cat_description, c.params AS cat_params, c.seotitle AS cat_seotitle"
			."\n FROM #__arthrology e"
			."\n INNER JOIN #__categories c ON c.id = e.catid"
			.$where_clause
			.$like_clause
			."\n ORDER BY e.year DESC, c.title ASC, e.title ASC";
		$database->setQuery($query, '#__', $pageNav->limit, $pageNav->limitstart);
		$rows = $database->loadObjectList();	

		clsArthrologyHTML::showResultsHTML($this->keyword, $this->catid, $rows, $total, $pageNav);

	}

	/***********************/
	/*  Make strings safe  */
	/***********************/
	public function makesafe($string='', $strict=1) {
		// Special thanks to Ioannis <datahell> Sannos for this
		
	    if ($string == '') { return $string; }
        if ($strict) {
            $pat = "([\']|[\!]|[\(]|[\)]|[\;]|[\"]|[\$]|[\#]|[\<]|[\>]|[\*]|[\%]|[\~]|[\`]|[\^]|[\|]|[\{]|[\}]|[\\\])";
        } else {
            $pat = "([\']|[\"]|[\$]|[\#]|[\<]|[\>]|[\*]|[\%]|[\~]|[\`]|[\^]|[\|]|[\{]|[\}]|[\\\])";
        }
        $s = eUTF::utf8_trim(preg_replace($pat, '', $string));
        return $s;
	}
}

// Initiate the class and execute it, then unset the 
// object in order to free the allocated PHP memory.
$objArthrology = new clsArthrology();
$objArthrology->main();
unset($objArthrology);
?>
