<?php 

/*
 * Arthrology for Elxis CMS 2008.x and 2009.x
 *
 * Installer Handler
 *
 * @version		1.3
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @authorurl	http://www.bitcraft-labs.gr
 * @copyright	Copyright (C) 2009-2011 Apostolos Koutsoulelos. All rights reserved.
 * @license		GNU/GPL (http://www.gnu.org/copyleft/gpl.html)
 * 
 * @link		http://www.elxis-downloads.com/downloads/miscellaneous/204.html
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Initialize variables

// Function automatically triggered by Elxis to fire the installation process 
function com_install() {
	global $database, $alang, $mainframe, $fmanager;

	// Load and execute adodb xml schema file
    if (file_exists($mainframe->getCfg('absolute_path').'/includes/adodb/adodb-xmlschema03.inc.php')) {
    	require_once($mainframe->getCfg('absolute_path').'/includes/adodb/adodb-xmlschema03.inc.php');
    	$schemafile = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/schema/arthrologyschema03.xml';
    } else {
    	require_once($mainframe->getCfg('absolute_path').'/includes/adodb/adodb-xmlschema.inc.php');
    	$schemafile = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/schema/arthrologyschema.xml';
    }
    $schema = new adoSchema($database->_resource);
    $schema->ContinueOnError(true);
    $schema->SetPrefix($mainframe->getCfg('dbprefix'));
    $schema->ParseSchema($schemafile);
    $schema->ExecuteSchema();

	//Load component's language
    $ipath = $mainframe->getCfg('absolute_path').'/components/com_arthrology';
	if (file_exists($ipath.'/language/'.$alang.'.php')) {
		require_once($ipath.'/language/'.$alang.'.php');
	} else if (file_exists($ipath.'/language/'.$mainframe->getCfg('alang').'.php')) {
		require_once($ipath.'/language/'.$mainframe->getCfg('alang').'.php');
	} else { 
		require_once($ipath.'/language/english.php');
	}
	$lng = new clsArthrologyLng();

	$install_errors = array();
	$install_criticals = array();

	// Set default parameters
	$query = "UPDATE #__components SET params='".
			"newauto=\n".
			"newall=\n".
			"changesauto=\n".
			"changesall=\n".
			"limit=25\n'".
			" WHERE admin_menu_link='option=com_arthrology' AND link='option=com_arthrology'";
	$database->setQuery($query);
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_PARAMS; }

	// Set up new icons for admin menu
	//		Control Panel
	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/controlpanel.png' WHERE admin_menu_link='option=com_arthrology'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_CP; }
	//		Arthrology
	$database->setQuery("UPDATE #__components SET admin_menu_img='../administrator/components/com_arthrology/images/arthrology_menu.png' WHERE admin_menu_link='option=com_arthrology' AND link='option=com_arthrology'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_MAIN; }
	//		Articles
	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/edit.png' WHERE admin_menu_link='option=com_arthrology&task=list'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_ART; }
	//		Magazines
	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/categories.png' WHERE admin_menu_link='option=com_categories&section=com_arthrology'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_MAG; }
	//		Configuration
	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/config.png' WHERE admin_menu_link='option=com_arthrology&task=conf&hidemainmenu=1'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_CONF; }

	// Add admin language
	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/language/english.com_arthrology.php';
	$destination = $mainframe->getCfg('absolute_path').'/administrator/language/english/english.com_arthrology.php';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_criticals[] = $lng->INS_CRITICAL_LNG_ENGLISH; }

	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/language/greek.com_arthrology.php';
	$destination = $mainframe->getCfg('absolute_path').'/administrator/language/greek/greek.com_arthrology.php';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_criticals[] = $lng->INS_CRITICAL_LNG_GREEK; }

	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/language/italian.com_arthrology.php';
	$destination = $mainframe->getCfg('absolute_path').'/administrator/language/italian/italian.com_arthrology.php';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_criticals[] = $lng->INS_CRITICAL_LNG_ITALIAN; }

	// Add SEO Pro functionality
	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/seopro.com_arthrology.php';
	$destination = $mainframe->getCfg('absolute_path').'/includes/seopro/com_arthrology.php';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_errors[] = $lng->INS_ERROR_SEOPRO; }
	
	// Add IOS Sitempap extension
	if ( (file_exists($mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/')) && (!file_exists($mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/extensions/arthrology.sitemap.php')) )  {
		$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/arthrology.sitemap.php';
		$destination = $mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/extensions/arthrology.sitemap.php';
		$success = $fmanager->copyFile($source, $destination);
		if (!$success) { $install_errors[] = $lng->INS_ERROR_SITEMAP; }
	}
	
	// Extend toolbar icons
	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/images/arthrology_articles.png';
	$destination = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_articles.png';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_errors[] = $lng->INS_ERROR_TOOLBAR; }
	
	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/images/arthrology_articles_f2.png';
	$destination = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_articles_f2.png';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_errors[] = $lng->INS_ERROR_TOOLBAR; }

	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/images/arthrology_magazines.png';
	$destination = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_magazines.png';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_errors[] = $lng->INS_ERROR_TOOLBAR; }
	
	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/images/arthrology_magazines_f2.png';
	$destination = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_magazines_f2.png';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_errors[] = $lng->INS_ERROR_TOOLBAR; }

	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/images/arthrology_config.png';
	$destination = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_config.png';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_errors[] = $lng->INS_ERROR_TOOLBAR; }
	
	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_arthrology/images/arthrology_config_f2.png';
	$destination = $mainframe->getCfg('absolute_path').'/administrator/images/arthrology_config_f2.png';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_errors[] = $lng->INS_ERROR_TOOLBAR; }

	?>

	<table class="adminheading">
		<tr>
			<th class="install"><?php echo $lng->INS_HEADER; ?></th>
		</tr>
	</table>

	<table class="adminform" style="empty-cells:hide">
		<tr>
			<th colspan="2"><?php echo $lng->INS_TITLE; ?></th>
		</tr>
		<tr>
			<td colspan="2"><?php echo $lng->INS_BODY; ?></td>
		</tr>
		<?php if (count($install_errors) > 0) { ?>
			<tr>
				<td colspan="2" class="menudottedline" style="background-color:#FF8686;">
					<?php echo $lng->INS_ERROR_NOTICE_TITLE; ?>:
				</td>
			</tr><tr>
				<td colspan="2">
					<?php 
					foreach ($install_errors as $install_error) {
						echo $install_error."<br />\n";
					}
					echo $lng->INS_ERROR_NOTICE;
					?>
				</td>
			</tr>
		<?php }	?>
		<?php if (count($install_criticals) > 0) { ?>
			<tr>
				<td colspan="2" class="menudottedline" style="background-color:#FF8686;">
					<?php echo $lng->INS_CRITICAL_NOTICE_TITLE; ?>:
				</td>
			</tr><tr>
				<td colspan="2">
					<?php 
					foreach ($install_criticals as $install_critical) {
						echo $install_critical."<br />\n";
					}
					echo $lng->INS_CRITICAL_NOTICE;
					?>
				</td>
			</tr>
		<?php }	?>
	</table>
<?php } ?>
